import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup } from "../../components";
import { Chip } from "../../components";

function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="Chip">
                    <Chip text="Short" image={CONST.IMAGE} />
                    <Chip text="Short" image={CONST.IMAGE} onCloseClick={onClick} />
                    <Chip text="Short" onCloseClick={onClick} />
                    <Chip text="Short" />
                    <Chip text="Long long long long long long long long long long long long" image={CONST.IMAGE} />
                    <Chip text="WithoutspaceWithoutspaceWithoutspaceWithoutspaceWithoutspace" image={CONST.IMAGE} />
                </PageSimple>
            </PageGroup>
        );
    }

}