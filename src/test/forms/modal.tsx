import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup, Page, PageHeader, PageContent, PageFooter } from "../../components";
import { Settings, SettingItem } from "../../components";
import { ButtonGroup, IconButton } from "../../components";
import { ModalMessage } from "../../components";
import { Modal } from "../../components";
import {IModalMessageState} from "../../components/modal/message";

function onClick() {
    alert("Click success!");
}

interface IFormProps { }
interface IFormState { }

const MESSAGE = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

function onResult(res: boolean) {
    alert(`Result: ${res}`);
}

export default class Form extends React.Component<{}, {}> {

    protected modalMessage: ModalMessage;
    protected modalCustom: Modal;

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="ModalMessage">
                    <Settings>
                        <SettingItem name="Default Info" description="">
                            <IconButton icon="send" onClick={() =>
                                this.modalMessage.show({
                                    message: MESSAGE,
                                    onResult: onResult
                                } as IModalMessageState)
                            }>Info</IconButton>
                        </SettingItem>
                        <SettingItem name="Info" description="Диалоговое окно с заданным заголовком">
                            <IconButton icon="send" onClick={() => {
                                this.modalMessage.show({
                                    type: "info",
                                    label: "Custom name",
                                    message: MESSAGE,
                                    onResult: onResult
                                } as IModalMessageState);
                            } }>Info</IconButton>
                        </SettingItem>
                        <SettingItem name="Error">
                            <IconButton icon="send" onClick={() => {
                                this.modalMessage.show({
                                    type: "error",
                                    message: MESSAGE,
                                    onResult: onResult
                                } as IModalMessageState);
                            } }>Info</IconButton>
                        </SettingItem>
                        <SettingItem name="Warn">
                            <IconButton icon="send" onClick={() => {
                                this.modalMessage.show({
                                    type: "warn",
                                    message: MESSAGE,
                                    onResult: onResult
                                } as IModalMessageState);
                            } }>Info</IconButton>
                        </SettingItem>
                        <SettingItem name="Question">
                            <IconButton icon="send" onClick={() => {
                                this.modalMessage.show({
                                    type: "question",
                                    message: MESSAGE,
                                    onResult: onResult
                                } as IModalMessageState);
                            } }>Info</IconButton>
                        </SettingItem>
                    </Settings>
                    <ModalMessage ref={e => this.modalMessage = e as ModalMessage} />
                </PageSimple>

                <PageSimple label="ModalMessage">
                    <Settings>
                        <SettingItem name="Custom">
                            <IconButton icon="send" onClick={() => this.modalCustom.show()} />
                        </SettingItem>
                    </Settings>
                    <Modal className="modal-custom" ref={e => this.modalCustom = e as Modal}>
                        <PageHeader
                            bgColor="main-color"
                            color="white"
                            >
                            <span>Title</span>
                            <IconButton icon="error" color="white" />
                        </PageHeader>
                        <PageContent bgColor="red">
                            <Settings>
                                <SettingItem name="First"><IconButton icon="insert_emoticon"></IconButton></SettingItem>
                                <SettingItem name="Second"><IconButton icon="insert_emoticon"></IconButton></SettingItem>
                                <SettingItem name="Third"><IconButton icon="insert_emoticon"></IconButton></SettingItem>
                            </Settings>
                        </PageContent>
                        <PageFooter bgColor="green">
                            <ButtonGroup>
                                <IconButton icon="error"></IconButton>
                                <IconButton icon="warning"></IconButton>
                                <IconButton icon="insert_emoticon"></IconButton>
                            </ButtonGroup>
                        </PageFooter>
                    </Modal>
                </PageSimple>

            </PageGroup>
        );
    }

}