import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup } from "../../components";
import { Settings, SettingItem } from "../../components";
import { InputField, InputAvatar, CheckBox, InputSelect, Toggle, InputRadio } from "../../components";

function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="InputField">
                    <InputField icon="phone" label="Error" description="description" error="Wrong description" fontSize="title"/>
                    <InputField icon="person" label="Text Label" description="description"/>
                    <InputField icon="person" label="Text Label"/>
                    <InputField defaultValue="Some value" />
                    <InputField defaultValue="Disabled" disabled icon="phone" description="description"/>
                    <InputField defaultValue="Disabled" disabled label="Test label" icon="phone" description="description"/>
                    <InputField placeholder="PlaceHolder" />
                    <InputField defaultValue="Some text" label="Label for input field" />
                    <InputField defaultValue="Some text" label="Label for input field long long long long long long long long long long long long long long" />
                    <InputField defaultValue="Some text" label="Label" description="Short description" />
                    <InputField defaultValue="Some text" label="Label" description="Description long long long long long long long long long long long long" />
                    <InputField defaultValue="Some text" label="Label" error="Short error" />
                    <InputField defaultValue="Some text" label="Label" error="Error long long long long long long long long long long long long long long long long" />
                    <InputField defaultValue="Some text" label="Label" error="error error error error error error error error error" description="desc desc desc desc desc desc desc desc desc desc desc desc" />
                    <InputField defaultValue="Some text" label="readOnly" readOnly />
                    <InputField label="readOnly" readOnly />
                    <SettingItem name="Whith Property Name + Label">
                        <InputField label="Label" description="description"/>
                    </SettingItem>
                </PageSimple>
                <PageSimple label="CheckBox">
                    <Settings>
                        <CheckBox label="Label" />
                        <CheckBox label="With Label DefaultChecked" defaultChecked/>
                        <CheckBox label="With Label Checked" checked/>
                        <CheckBox label="With Label Disabled" disabled/>
                        <CheckBox label="With Label Checked Disabled" checked disabled/>
                        <SettingItem name="Large withOut label">
                            <CheckBox/>
                        </SettingItem>
                        <CheckBox label="Label long long long long long long long long long long long long long long long long" />
                        <SettingItem name="Large without label">
                            <CheckBox/>
                        </SettingItem>
                        <SettingItem name="Whith Property Name + Label + defaultChecked">
                            <CheckBox label="Label" defaultChecked />
                        </SettingItem>
                        <SettingItem name="Large onChange">
                            <CheckBox onChange={e => alert(e.currentTarget.checked)} label="Click me!" />
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="InputSelect">
                    <Settings>
                        <InputSelect>
                            <div>First element</div>
                            <div>Second element</div>
                            <div>Third element</div>
                        </InputSelect>
                        <SettingItem name="Select">
                            <InputSelect>
                                <div>First element</div>
                                <div>Second element</div>
                                <div>Third element</div>
                            </InputSelect>
                        </SettingItem>
                        <SettingItem name="Disabled">
                            <InputSelect>
                                <div>First element</div>
                                <div>Second Long Long Long Long Long Long element</div>
                                <div>Third element</div>
                            </InputSelect>
                        </SettingItem>
                        <SettingItem name="Large list">
                            <InputSelect>
                                <div>First element long long long long long long long</div>
                                <div>Second element</div>
                                <div>Third element</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                                <div>More</div>
                            </InputSelect>
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="Switch Toogle" >
                    <Settings>
                        <SettingItem name="Default">
                            <Toggle ></Toggle>
                        </SettingItem>
                        <SettingItem name="Default checked">
                            <Toggle defaultChecked />
                        </SettingItem>
                        <SettingItem name="Disabled">
                            <Toggle disabled />
                        </SettingItem>
                        <SettingItem name="Disabled checked">
                            <Toggle disabled defaultChecked/>
                        </SettingItem>
                        <SettingItem name="On change">
                            <Toggle onChange={e => alert(e.currentTarget.checked)} />
                        </SettingItem>
                    </Settings>
                </PageSimple>
                 <PageSimple label="InputField">
                    <Settings>
                        <InputAvatar defaultImage={CONST.IMAGE} onChange={() => alert("Image changed")} />
                        <SettingItem name="Default image">
                            <InputAvatar defaultImage={CONST.IMAGE} onChange={() => alert("Image changed")} />
                        </SettingItem>
                        <SettingItem name="Small size">
                            <InputAvatar defaultImage={CONST.IMAGE} onChange={() => alert("Image changed")} size="small" />
                        </SettingItem>
                        <SettingItem name="Medium size">
                            <InputAvatar defaultImage={CONST.IMAGE} onChange={() => alert("Image changed")} size="medium" />
                        </SettingItem>
                        <SettingItem name="Large size">
                            <InputAvatar defaultImage={CONST.IMAGE} onChange={() => alert("Image changed")} size="large" />
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="InputField">
                   <InputRadio className="test" label="Test label"/>
                   <InputRadio className="test" label="Max length? Test label long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long"/>
                   <InputRadio label="Test label" disabled={true}/>
                   <InputRadio label="Test label" disabled={false}/>
                   <InputRadio label="Test label"/>
                   <p>Группы</p>
                   <div>
                       <InputRadio name="group1" label="Test label1 group1" id="test_id"/>
                       <InputRadio name="group1" label="Test label2 group1" defaultChecked onChange={onClick}/>
                       <InputRadio name="group1" label="Test label3 group1"/>
                       <InputRadio name="group1" label="Test label4 group1"/>
                   </div>
                   <div>
                       <InputRadio name="group2" label="Test label1 group2" />
                       <InputRadio name="group2" label="Test label2 group2" defaultChecked onChange={onClick}/>
                       <InputRadio name="group2" label="Test label3 group2" className={"Test"} />
                       <InputRadio name="group2" label="Test label4 group2" disabled />
                   </div>
                   <p>onClick</p>
                   <InputRadio label="Test click" onClick={onClick}/>
                   <p>onChange</p>
                   <InputRadio label="Test click" onChange={onClick}/>
                   <p>set value: defaultValue & value</p>
                   <InputRadio label="Test click" defaultValue={"Val def"}/>
                   <InputRadio label="Test click" value={"value"}/>
                   <p>disabled</p>
                   <InputRadio label="disabled checked" name="group4" checked disabled />
                   <InputRadio label="disabled" name="group4" disabled />
                   <p>Заметка: в группе radio input-ов не использовать "checked disabled". Только не привязанным к группам input-ам </p>
                   <InputRadio label="Test click" name="group3" />
                   <InputRadio label="Test click" name="group3" id="qwerty"/>
                   <InputRadio label="Test click" name="group3" checked disabled />

                </PageSimple>
            </PageGroup>
        );
    }

}