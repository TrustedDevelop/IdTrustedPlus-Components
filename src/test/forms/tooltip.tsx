import * as React from "react";

// Components
import {RaisedButton, FlatButton, CopyButton, RoundButton, ButtonGroup} from "../../components";
import {IconButton, RefreshButton, AddButton, MoreButton, PhotoButton} from "../../components";
import {ActionButton} from "../../components";
import {PageSimple, PageGroup, Page, PageHeader, PageContent} from "../../components";
import {Icon} from "../../components";
import {Settings, SettingItem} from "../../components";
import {Title, Text} from "../../components";
import {List, ListCell, ListItem} from "../../components/list/index";


function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        const onClick = () => void 0;
        return (
            <PageGroup type="col">
                <Page>
                    <ButtonGroup>
                        <IconButton tooltip="tooltip-top" icon="person" color="blue"/>
                        <IconButton tooltip="tooltip-right" tooltipPosition="right" icon="person" color="blue"/>
                        <IconButton tooltip="tooltip-bottom" tooltipPosition="bottom" icon="person" color="blue"/>
                        <IconButton tooltip="tooltip-left" tooltipPosition="left" icon="person" color="blue"/>
                    </ButtonGroup>
                </Page>
                <Page>
                    <PageContent>
                        <ButtonGroup>
                            <RaisedButton position="center" tooltip="tooltip top">Center RaisedButton</RaisedButton>
                            <RaisedButton position="center" tooltip="tooltip right" tooltipPosition="right">Center RaisedButton</RaisedButton>
                            <RaisedButton position="center" tooltip="tooltip bottom" tooltipPosition="bottom">Center RaisedButton</RaisedButton>
                            <RaisedButton position="center" tooltip="tooltip left" tooltipPosition="left">Center RaisedButton</RaisedButton>
                        </ButtonGroup>
                    </PageContent>
                </Page>
                <PageSimple label="CopyButton">
                    <Settings>
                        <SettingItem name="Copy to buffer" description="Must be 'Text from buffer'">
                            <CopyButton text="Text from buffer" tooltip="Text from buffer" onClick={onClick}/>
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="List">
                    <List>
                        <ListItem tooltip="tooltip in list">
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                        </ListItem>
                        <ListItem tooltip="tooltip in list" tooltipPosition="right">
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                        </ListItem>
                        <ListItem tooltip="tooltip in list" tooltipPosition="bottom">
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                        </ListItem>
                        <ListItem tooltip="tooltip in list" tooltipPosition="left">
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                        </ListItem>
                    </List>
                </PageSimple>
                <ActionButton onClick={onClick}><Icon name="add"/></ActionButton>
            </PageGroup >
        );
    }

};