import * as React from "react";

// Components
import { PageSimple, PageGroup } from "../../components";
import { Settings, SettingItem } from "../../components";
import { Icon, IconAdd, IconEdit, IconMenu, IconMore, IconPhoto, IconRefresh } from "../../components";

function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="Icons">
                    <Settings>
                        <SettingItem name="Default color icon" ><Icon name="search" /></SettingItem>
                        <SettingItem name="Gray color icon"><Icon name="clear" color="gray" /></SettingItem>
                        <SettingItem name="IconAdd"><IconAdd /></SettingItem>
                        <SettingItem name="IconEdit"><IconEdit /></SettingItem>
                        <SettingItem name="IconMenu"><IconMenu /></SettingItem>
                        <SettingItem name="IconMore"><IconMore /></SettingItem>
                        <SettingItem name="IconPhoto"><IconPhoto /></SettingItem>
                        <SettingItem name="IconRefresh"><IconRefresh /></SettingItem>
                    </Settings>
                </PageSimple>
            </PageGroup>
        );
    }

}