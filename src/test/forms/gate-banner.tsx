import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup } from "../../components";
import { Chip, InputField, Icon, InputRadio, SettingItem, Toggle, Text, ButtonGroup, RaisedButton } from "../../components";

function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <div className="banner">
            <PageGroup type="col" className="banner">
                <PageSimple label="Баннер">
                    <SettingItem name="Использовать собственный баннер">
                            <Toggle ></Toggle>
                    </SettingItem>
                </PageSimple>
            <PageGroup type="row">
                <div className="banner_settings">
                <PageSimple label="Настройки" className="banner_settings">
                    <InputRadio name="load_banner" label="Картинка + ссылка" />
                    <InputField icon="link" label="URL" description="Ссылка с вашего банера"/>
                    <InputField icon="insert_photo" label="File" description="Выберите картинку на ваш банер"/>
                    <InputRadio name="load_banner" label="Код динамического баннера" />
                    <Text>
                        Введите код для вставки баннера
                    </Text>
                    <ButtonGroup>
                        <RaisedButton position="right">Сохранить</RaisedButton>
                    </ButtonGroup>
                </PageSimple>
                </div>
                <PageSimple label="Предпросмотр">
                    <div className="content-div">
                        <a href="#">
                            <img src="../banner.jpg"/>
                        </a>
                    </div>
                </PageSimple>
            </PageGroup>
            </PageGroup>
            </div>
        );
    }

}