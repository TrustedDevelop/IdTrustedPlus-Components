import * as React from "react";
import { hashHistory } from "react-router";

// Components
import { PageSimple, PageGroup, Text } from "../../components";

export interface IFormProps {
    params: {
        name: string;
    };
}

export default class Form extends React.Component<IFormProps, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="TabBar demo">
                    <Text fontSize="title">{this.props.params.name}</Text>
                </PageSimple>
            </PageGroup>
        );
    }

}