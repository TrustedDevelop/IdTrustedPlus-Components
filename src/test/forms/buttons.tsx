import * as React from "react";

// Components
import { RaisedButton, FlatButton, CopyButton, RoundButton, ButtonGroup } from "../../components";
import { IconButton, RefreshButton, AddButton, MoreButton, PhotoButton } from "../../components";
import { ActionButton } from "../../components";
import { PageSimple, PageGroup, Page, PageHeader, PageContent} from "../../components";
import { Icon } from "../../components";
import { Settings, SettingItem } from "../../components";
import { Title, Text } from "../../components";


function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {

        const simple_text = <p>Text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text</p>;

        return (
            <PageGroup type="col">
                <Title label="Абсолютное позиционирование блоков кнопок внутри Pages"/>
                <Page>
                    <PageHeader bgColor="blue" size="medium">
                        <Title label="Top Icons Button" description="справа и слева" color="white"/>
                        <ButtonGroup position="top" align="left">
                            <IconButton color="white" icon="add"/>
                            <IconButton icon="clear" color="white"/>
                            <IconButton icon="person" color="white"/>
                        </ButtonGroup>
                        <ButtonGroup position="top" align="right">
                            <IconButton color="white" icon="add"/>
                            <IconButton icon="clear" color="white"/>
                            <IconButton icon="person" color="white"/>
                        </ButtonGroup>
                        <ButtonGroup position="bottom" align="right">
                            <IconButton color="white" icon="add"/>
                            <IconButton icon="clear" color="white"/>
                            <IconButton icon="person" color="white"/>
                        </ButtonGroup>
                    </PageHeader>
                </Page>
                <Title label="Позиционирование кнопок внутри блока группой"/>
                <Page>
                    <ButtonGroup align="left">
                        <IconButton color="green" icon="add"/>
                        <IconButton icon="clear" color="red"/>
                        <IconButton icon="person" color="blue"/>
                    </ButtonGroup>
                    <ButtonGroup align="center">
                        <IconButton icon="add"/>
                        <IconButton icon="clear"/>
                        <IconButton icon="person"/>
                    </ButtonGroup>
                    <ButtonGroup align="right">
                        <IconButton icon="add"/>
                        <IconButton icon="clear"/>
                        <IconButton icon="person"/>
                    </ButtonGroup>
                </Page>
                <Title label="Позиционирование кнопок внутри блока"/>
                <Page>
                    <PageContent>
                        <ButtonGroup>
                            <RaisedButton position="right">Right RaisedButton</RaisedButton>
                            <RaisedButton position="right">Right RaisedButton</RaisedButton>
                            <RaisedButton position="left">Left RaisedButton</RaisedButton>
                        </ButtonGroup>
                        <ButtonGroup>
                            <RaisedButton position="right">Right RaisedButton</RaisedButton>
                            <RaisedButton position="left">Left RaisedButton</RaisedButton>
                            <RaisedButton position="center">Center RaisedButton</RaisedButton>
                        </ButtonGroup>
                        <ButtonGroup>
                            <RaisedButton position="right">Right RaisedButton</RaisedButton>
                            <RaisedButton position="left">Left RaisedButton</RaisedButton>
                            <RaisedButton position="left">Left RaisedButton</RaisedButton>
                        </ButtonGroup>
                        <ButtonGroup>
                            <RaisedButton position="center">Center RaisedButton</RaisedButton>
                            <RaisedButton position="center">Center RaisedButton</RaisedButton>
                            <RaisedButton position="center">Center RaisedButton</RaisedButton>
                        </ButtonGroup>
                    </PageContent>
                </Page>
                <Title label="Виды кнопок"/>
                <PageGroup type="row">
                    <PageSimple label="RaisedButton">
                        <Settings>
                            <SettingItem name="Enabled">
                                <RaisedButton onClick={onClick}>Normal</RaisedButton>
                            </SettingItem>
                            <SettingItem name="Disabled">
                                <RaisedButton onClick={onClick} disabled>Disabled</RaisedButton>
                            </SettingItem>
                            <SettingItem name="Short name">
                                <RaisedButton onClick={onClick}>S</RaisedButton>
                            </SettingItem>
                            <SettingItem name="Long name">
                                <RaisedButton onClick={onClick}>Long long long long long long long long long long</RaisedButton>
                            </SettingItem>
                            <SettingItem name="Enabled with bgColor & color">
                                <RaisedButton onClick={onClick} color="black" bgColor="white">Normal</RaisedButton>
                            </SettingItem>
                            <SettingItem name="Disabled with bgColor & color">
                                <RaisedButton onClick={onClick} color="black" bgColor="white" disabled>Disabled</RaisedButton>
                            </SettingItem>
                        </Settings>
                    </PageSimple>
                    <PageSimple label="FlatButton">
                        <Settings>
                            <SettingItem name="Enabled">
                                <FlatButton onClick={onClick} >Normal</FlatButton>
                            </SettingItem>
                            <SettingItem name="Disabled">
                                <FlatButton onClick={onClick} disabled>Disabled</FlatButton>
                            </SettingItem>
                            <SettingItem name="Short name">
                                <FlatButton onClick={onClick}>S</FlatButton>
                            </SettingItem>
                            <SettingItem name="Long name">
                                <FlatButton onClick={onClick}>Long long long long long long long long long long</FlatButton>
                            </SettingItem>

                            <SettingItem name="Enabled with bgColor & color">
                                <FlatButton onClick={onClick} color="orange" >Normal</FlatButton>
                            </SettingItem>
                            <SettingItem name="Disabled with bgColor & color">
                                <FlatButton onClick={onClick} color="orange" disabled>Disabled</FlatButton>
                            </SettingItem>
                        </Settings>
                    </PageSimple>
                </PageGroup>
                <PageSimple label="CopyButton">
                    <Settings>
                        <SettingItem name="Copy to buffer" description="Must be 'Text from buffer'">
                            <CopyButton text="Text from buffer" onClick={onClick} />
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="RoundButton">
                    <Settings>
                        <SettingItem name="Enabled">
                            <RoundButton title="sample title" onClick={onClick}>Text</RoundButton>
                        </SettingItem>
                        <SettingItem name="Disabld">
                            <RoundButton title="sample title" disabled onClick={onClick}>Text</RoundButton>
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <PageSimple label="IconButton">
                    <Settings>
                        <SettingItem name="Enabled">
                            <IconButton title="sample title" color="red" onClick={onClick} icon="search" />
                        </SettingItem>
                        <SettingItem name="Disabled">
                            <IconButton title="sample title" color="blue" disabled onClick={onClick} icon="search" />
                        </SettingItem>
                        <SettingItem name="Small">
                            <IconButton title="sample title" color="orange" onClick={onClick} icon="smartphone" size="small" />
                        </SettingItem>
                        <SettingItem name="Medium">
                            <IconButton title="sample title" color="green" onClick={onClick} icon="tablet" size="medium" />
                        </SettingItem>
                        <SettingItem name="Large">
                            <IconButton title="sample title" onClick={onClick} icon="tv" size="large" />
                        </SettingItem>
                        <SettingItem name="RefreshButton">
                            <RefreshButton title="sample title" onClick={onClick} />
                        </SettingItem>
                        <SettingItem name="AddButton">
                            <AddButton title="sample title" onClick={onClick} />
                        </SettingItem>
                        <SettingItem name="MoreButton">
                            <MoreButton title="sample title" onClick={onClick} />
                        </SettingItem>
                        <SettingItem name="PhotoButton">
                            <PhotoButton onClick={onClick} title="Фото" />
                        </SettingItem>
                    </Settings>
                </PageSimple>
                <ActionButton onClick={onClick}><Icon name="add" /></ActionButton>
            </PageGroup >
        );
    }

}