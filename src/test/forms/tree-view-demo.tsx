import * as React from "react";

// Components
import {
    List,
    ListHeader,
    ListHeaderItem,
    PageGroup,
    PageSimple,
    Text,
    TreeViewContainer
} from "../../components";

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="Tree view">
                    <TreeViewContainer label="Click me!">
                        <Text>Simple text 1.</Text>
                        <Text>Simple text 2.</Text>
                        <Text>Simple text 3.</Text>
                        <Text>Simple text 4.</Text>
                        <TreeViewContainer label="Show new page">
                            <PageSimple label="New page">
                                <TreeViewContainer label="Click me!">
                                    <Text>Simple text 1.</Text>
                                    <Text>Simple text 2.</Text>
                                    <Text>Simple text 3.</Text>
                                    <TreeViewContainer label="disabled page" disabled>
                                        <PageSimple label="New page">
                                            <TreeViewContainer label="Click me!">
                                                <Text>Simple text 1.</Text>
                                                <Text>Simple text 2.</Text>
                                                <TreeViewContainer label="Show new page">
                                                    <PageSimple label="New page">
                                                        <Text>END</Text>
                                                    </PageSimple>
                                                </TreeViewContainer>
                                            </TreeViewContainer>
                                        </PageSimple>
                                    </TreeViewContainer>
                                </TreeViewContainer>
                            </PageSimple>
                        </TreeViewContainer>
                    </TreeViewContainer>
                </PageSimple>
            </PageGroup>
        );
    }

}