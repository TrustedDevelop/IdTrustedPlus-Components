import * as React from "react";

// Components
import { PageSimple, PageGroup } from "../../components";
import { Settings, SettingItem } from "../../components";
import { EditButton, PhotoButton } from "../../components";

function onClick() {
    alert("Click success!");
}

export default class Form extends React.Component<{}, {}> {

    render() {
        return (
            <PageGroup type="col">
                <PageSimple label="Settings">
                    <Settings>
                        <SettingItem name="First item">
                        </SettingItem>
                        <SettingItem name="Second item" description="Description for second item">
                            <span>+++</span>
                        </SettingItem>
                        <SettingItem name="Third item with long long long long long long long long long long long long name" description="Description for third item with long long long long long long long long long long long long">
                            <EditButton onClick={onClick}/>
                        </SettingItem>
                        <SettingItem name="Item with custom button">
                            <PhotoButton onClick={onClick} title="Фото"/>
                        </SettingItem>
                    </Settings>
                </PageSimple>
            </PageGroup>
        );
    }

}