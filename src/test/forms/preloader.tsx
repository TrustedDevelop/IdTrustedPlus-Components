import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup } from "../../components";
import { PreloaderCircle, PreloaderLine, PreloaderFlashingColors } from "../../components";
import { Text } from "../../components";

export default class Form extends React.Component<{}, {}> {

  render() {
    return (
      <PageGroup type="col">
        <PageSimple label="Preloader">
          <Text><h2>Круглый</h2>
            <p>small medium large</p>
            <PreloaderCircle size="small" color="orange" />
            <PreloaderCircle color="blue" hidden={true} />
            <PreloaderCircle size="large" color="red" />
            <h2>Circular Flashing Colors</h2>
            <p>small medium large</p>
            <PreloaderFlashingColors size="small" />
            <PreloaderFlashingColors />
            <PreloaderFlashingColors size="large" />
            <h2>Линейный</h2>
            <p>Indeterminate</p>
            <PreloaderLine />
            <PreloaderLine color="green" />
            <p>Determinate</p>
            example: width 70%
                    <PreloaderLine color="grey" type="determinate" style={{ width: "70%" }} />
            example: width 30%
                    <PreloaderLine color="deep-purple" type="determinate" style={{ width: "30%" }} />
          </Text>
        </PageSimple>
      </PageGroup>
    );
  }
}