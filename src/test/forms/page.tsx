import * as React from "react";

// Components
import { Page, PageGroup, PageHeader, PageContent } from "../../components";
import { Title, Text, InputField, RaisedButton, FlatButton, ButtonGroup } from "../../components";

export default class Form extends React.Component<{}, {}> {

    render() {

        const span = <span>Text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text</span>;

        return (
            <PageGroup type="col">
                <PageGroup type="row">
                    <Page>
                        <PageHeader bgColor="blue">
                            <Title color="white" label="Введите номер телефона"/>
                        </PageHeader>
                        <PageContent>
                            <InputField label="Номер телефона" description=" "/>
                            <ButtonGroup>
                                <RaisedButton position="right">Продолжить</RaisedButton>
                            </ButtonGroup>
                        </PageContent>
                    </Page>
                    <Page>
                        <PageHeader bgColor="blue">
                            <Title color="white" label="Введите проверочный код"/>
                        </PageHeader>
                        <PageContent>
                            <InputField label="Проверочный код" description=" "/>
                            <ButtonGroup>
                                <RaisedButton position="right">Готово</RaisedButton>
                                <FlatButton position="left" disabled>Новый код</FlatButton>
                            </ButtonGroup>
                        </PageContent>
                    </Page>
                </PageGroup>
                <PageGroup type="row">
                    <Page>
                        <PageHeader bgColor="blue">
                            <Title color="white" label="Введите номер телефона"/>
                        </PageHeader>
                        <PageContent>
                            <InputField label="Номер телефона" error="неверный формат номера"/>
                            <ButtonGroup>
                                <RaisedButton position="right">Продолжить</RaisedButton>
                            </ButtonGroup>
                        </PageContent>
                    </Page>
                    <Page>
                        <PageHeader bgColor="blue">
                            <Title color="white" label="Введите проверочный код"/>
                        </PageHeader>
                        <PageContent>
                            <InputField label="Проверочный код" error="Неверный код"/>
                            <ButtonGroup>
                                <RaisedButton position="right">Готово</RaisedButton>
                                <FlatButton position="left">Новый код</FlatButton>
                            </ButtonGroup>
                        </PageContent>
                    </Page>
                </PageGroup>
                <PageGroup type="col">
                    <Page>
                        <PageHeader>
                            <Title label="Title Default"></Title>
                        </PageHeader>
                        <PageContent className="wow" >
                            <Text>text default</Text>
                            <Text color="red">text red</Text>
                            <Text color="red" colorSheme="dark">Text color="red" colorSheme="dark"</Text>
                            <Text color="green" colorSheme="light">Text color="green" colorSheme="light"</Text>
                        </PageContent>
                    </Page>
                </PageGroup>
                <PageGroup type="col">
                    <Page>
                        <PageHeader bgImage="http://media.istockphoto.com/photos/abstract-cubes-retro-styled-colorful-background-picture-id508795172?k=6&m=508795172&s=170667a&w=0&h=uxKISA4xMNkrBCcEFhdN5mm-ZDv8LFzWUhMMmG3CNuQ=">
                            <Title label="display-4" fontSize="display-4"/>
                        </PageHeader>
                        <PageContent className="wow" >
                            <Title label="Title" description="description"/>
                            <Text color="red" fontSize="body-2" colorSheme="light">{span}</Text>
                            <Text color="red" fontSize="body-2" colorSheme="dark">{span}</Text>
                        </PageContent>
                    </Page>
                </PageGroup>
                <PageGroup type="col">
                    <Page>
                        <PageHeader bgColor="black">
                            <Title label="Title" colorSheme="light" color="blue" fontSize="display-3"/>
                        </PageHeader>
                        <PageContent>
                            <Text fontSize="body-1">font="body-1"</Text>
                            <Text fontSize="body-2">font="body-2"</Text>
                            <Text fontSize="display-1">font="display-1"</Text>
                            <Text fontSize="display-2">font="display-2"</Text>
                            <Text fontSize="display-3">font="display-3"</Text>
                            <Text fontSize="display-4">font="display-4"</Text>
                            <Text fontSize="headline">font="headline"</Text>
                            <Text fontSize="subheading">font="subheading"</Text>
                            <Text fontSize="caption">font="caption"</Text>
                            <Text fontSize="button">font="button"</Text>
                        </PageContent>
                    </Page>
                    <Page>
                       <PageHeader size="small" bgColor="blue">
                          <Title color="white" colorSheme="light" label="Title Display1 Small Header" fontSize="display-1" description="Тут могла быть ваша реклама"/>
                       </PageHeader>
                       <PageContent>
                          <Text>Какой-то контент</Text>
                       </PageContent>
                    </Page>
                    <Page>
                       <PageHeader size="medium" bgColor="orange">
                          <Title colorSheme="light" color="white" label="Title Display2 Medium Header" fontSize="display-2" description="Тут могла быть ваша реклама"/>
                       </PageHeader>
                       <PageContent>
                          <Text>Какой-то контент</Text>
                       </PageContent>
                    </Page>
                    <Page>
                       <PageHeader size="large" bgColor="green">
                          <Title colorSheme="light" color="white" label="Title Display3 Large Header" fontSize="display-3" description="Тут могла быть ваша реклама"/>
                       </PageHeader>
                       <PageContent>
                          <Text>Какой-то контент</Text>
                       </PageContent>
                    </Page>
                    <PageGroup type="row">
                        <Page>
                            <PageContent>
                                <Text>Без Титла, 3 столбика</Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageContent>
                                <Text>Без Титла, 3 столбика</Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageContent>
                                <Text>Без Титла, 3 столбика</Text>
                            </PageContent>
                        </Page>
                    </PageGroup>
                    <PageGroup type="row">
                        <Page>
                            <PageContent>
                                <Text>Без Титла, 2 столбика</Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageContent>
                                <Text>Без Титла, 2 столбика</Text>
                            </PageContent>
                        </Page>
                    </PageGroup>
                </PageGroup>
                <PageGroup type="row">
                    <PageGroup type="col">
                        <Page>
                            <PageHeader bgColor="red">
                              <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! </Text>
                            </PageContent>
                        </Page>
                        <PageGroup type="col">
                            <Page>
                                <PageHeader bgColor="green">
                                    <Title colorSheme="light" color="white" label="cтолбик в столбике"/>
                                </PageHeader>
                                <PageContent>
                                    <Text>Мало текста</Text>
                                </PageContent>
                            </Page>
                            <Page>
                                <PageHeader bgColor="green">
                                    <Title colorSheme="light" color="white" label="cтолбик в столбике"/>
                                </PageHeader>
                                <PageContent>
                                    <Text>Мало текста</Text>
                                </PageContent>
                            </Page>
                        </PageGroup>
                    </PageGroup>
                    <PageGroup type="col">
                        <Page>
                            <PageHeader bgColor="red">
                                <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! </Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageHeader bgColor="red">
                                <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! </Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageHeader bgColor="red">
                                <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! Много текста! </Text>
                            </PageContent>
                        </Page>
                    </PageGroup>
                    <PageGroup type="col">
                        <Page>
                            <PageHeader bgColor="red">
                                <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Мало текста</Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageHeader bgColor="red">
                                <Title colorSheme="light" color="white" label="3 столбика"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Мало текста</Text>
                            </PageContent>
                        </Page>
                        <PageGroup type="row">
                        <Page>
                           <PageHeader bgColor="orange">
                                <Title colorSheme="light" color="white" label="строка в столбике"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Мало текста</Text>
                            </PageContent>
                        </Page>
                        <Page>
                            <PageHeader bgColor="orange">
                                <Title colorSheme="light" color="white" label="строка в столбике"/>
                            </PageHeader>
                            <PageContent>
                                <Text>Мало текста</Text>
                            </PageContent>
                        </Page>
                    </PageGroup>
                    </PageGroup>
                </PageGroup>
            </PageGroup>

        );
    }

}