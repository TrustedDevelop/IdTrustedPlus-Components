import * as React from "react";

import * as CONST from "../const";

// Components
import { PageSimple, PageGroup, Page, PageContent, PageFooter, PageHeader } from "../../components";
import { List, ListAction, ListHeader, ListHeaderItem, ListItem, ListImage, ListIcon, ListName, ListHeaderCustomItem, TreeViewContainer, ListMenu, ListCell, ListMore } from "../../components";
import { MenuItem } from "../../components";
import { Text, Title } from "../../components";
import { IconButton } from "../../components";
// need it?

export default class Form extends React.Component<{}, {}> {

    onClick() {
        alert("Click success!");
    }
    onItemClick(e: any) {
        alert(e);
    }

    render() {
        const arr = (Array.apply(null, Array(20)) as number[]).map((v, i) => i + 1);
        const arr2 = (Array.apply(null, Array(5)) as number[]).map((v, i) => i + 1);
        return (
            <PageGroup type="col">
                <Title label="История пользователя Trusted.Net" />
                <Page>
                    <List className="user-history">
                        <ListItem to="https://cryptostore.ru">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="CryptoStore.ru" description="Интернет-магазин CryptoStore.ru" />
                            <ListCell>https://cryptostore.ru</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>
                        <ListItem to="https://cryptostore.ru">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="CryptoStore.ru" description="Интернет-магазин CryptoStore.ru" />
                            <ListCell>https://cryptostore.ru</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>
                        <ListItem to="https://cryptostore.ru">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="CryptoStore.ru" description="Длинное описание!!! Длинное описание!!! Длинное описание!!! Длинное описание!!! Длинное описание!!! Длинное описание!!! Длинное описание!!! " />
                            <ListCell>https://cryptostore.ru</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>
                        <ListItem to="https://net.trusted.ru/license/authorize">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="JWT License" description="Генерилка ключей Trusted" />
                            <ListCell>https://net.trusted.ru/license/authorize</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>
                        <ListItem to="https://net.trusted.ru/license/authorize">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="JWT License" description="Генерилка ключей Trusted" />
                            <ListCell>https://net.trusted.ru/andMore/andMore/andMore/andMore/andMore/andMore/andMore/andMore/andMore/andMore/andMore/authorize</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>
                        <ListItem to="https://net.trusted.ru/license/authorize">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="JWT License" description="Генерилка ключей Trusted" />
                            <ListCell>https://net.trusted.ru/license/authorize</ListCell>
                            <ListCell className="date">01.01.2016 11:22</ListCell>
                        </ListItem>

                    </List>
                </Page>
                <Title label="Лист с иконками, ссылкми на элементах" />
                <Page>
                    <List>
                        <ListItem to="#">
                            <ListIcon color="red" icon="language"></ListIcon>
                            <ListName label="Лист Итем 1" description="description" />
                            <ListCell>произвольные данные</ListCell>
                        </ListItem>
                        <ListItem to="#">
                            <ListIcon color="blue" icon="person"></ListIcon>
                            <ListName label="Лист Итем 2" description="description" />
                            <ListCell>произвольные данные</ListCell>
                        </ListItem>
                        <ListItem to="#">
                            <ListIcon color="green" icon="person"></ListIcon>
                            <ListName label="Лист Итем 3" description="description" />
                            <ListCell>произвольные данные</ListCell>
                        </ListItem>
                    </List>
                </Page>
                <Title label="Лист с иконками, действием на элементах" />
                <Page>
                    <List>
                        <ListItem>
                            <ListIcon color="red" icon="person"></ListIcon>
                            <ListName label="Лист Итем 1" description="description" />
                            <ListCell>произвольные данные длиииииииииииииииииииннннныыыыыыыееееееее данннннныыыыыыееееее</ListCell>
                            <ListAction icon="clear" color="red" onClick={this.onClick}></ListAction>
                        </ListItem>
                        <ListItem>
                            <ListIcon color="blue" icon="person"></ListIcon>
                            <ListName label="Лист Итем 2222" description="description" />
                            <ListCell>произвольные данные</ListCell>
                            <ListAction icon="clear" color="blue" onClick={this.onClick}></ListAction>
                        </ListItem>
                        <ListItem>
                            <ListIcon color="green" icon="person"></ListIcon>
                            <ListName label="Лист Итем 3333333333" description="description" />
                            <ListCell>произвольные данные</ListCell>
                            <ListAction icon="clear" onClick={this.onClick}></ListAction>
                        </ListItem>
                    </List>
                </Page>
                <Title label="Лист с аватарками, onClick на элементах списка и меню " />
                <Page>
                    <List>
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListName label="Лист Итем 1" description="description" />
                            <ListCell>произвольные данные</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListName label="Лист Итем 2" description="description" />
                            <ListCell>произвольные данные</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2 Long Long Long Long Long Long Long Long Long Long LongLong Long Long Long Long Long Long Long Long Long Long" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListName label="Лист Итем 3" description="description" />
                            <ListCell>произвольные данные</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                    </List>
                </Page>
                <Title label="Тонкий лист с аватарками, onClick на элементах списка и меню " />
                <Page>
                    <List className="thin">
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListImage image={CONST.IMAGE} />
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListMenu>
                                <MenuItem text="Menu1" />
                                <MenuItem text="Menu2" />
                                <MenuItem text="Menu3" />
                            </ListMenu>
                        </ListItem>
                    </List>
                </Page>
                <Title label="Тонкий лист с иконками, onClick на элементах списка" />
                <Page>
                    <List className="thin">
                        <ListItem onClick={this.onClick}>
                            <ListIcon icon="person" color="red"/>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListCell>произвольные данные3</ListCell>
                            <ListCell>произвольные данные4</ListCell>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListIcon icon="person" color="green"/>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListCell>произвольные данные3</ListCell>
                            <ListCell>произвольные данные4</ListCell>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListIcon icon="person" color="blue"/>
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListCell>произвольные данные3</ListCell>
                            <ListCell>произвольные данные4</ListCell>
                        </ListItem>
                        <ListItem onClick={this.onClick}>
                            <ListIcon icon="person" />
                            <ListCell>произвольные данные1</ListCell>
                            <ListCell>произвольные данные2</ListCell>
                            <ListCell>произвольные данные3</ListCell>
                            <ListCell>произвольные данные4</ListCell>
                        </ListItem>
                    </List>
                </Page>
            </PageGroup>
        );
    }

}