import * as React from "react";
import * as ReactDOM from "react-dom";
import { hashHistory, Route, Router, Redirect } from "react-router";

// Components
import { AppBarSimple, AppLeftMenu, AppLeftMenuContent, AppLeftMenuHeader, AppLeftMenuFooter, MenuItem, Footer, TabBar, ITabItem, TreeViewContainer, Title } from "../components";

// Forms
import Button from "./forms/buttons";
import Page from "./forms/page";
import Chip from "./forms/chip";
import Icon from "./forms/icon";
import Input from "./forms/input";
import List from "./forms/list";
import Table from "./forms/table";
import Preloader from "./forms/preloader";
import Settings from "./forms/settings";
import Stepper from "./forms/stepper";
import Modal from "./forms/modal";
import TreeViewDemo from "./forms/tree-view-demo";
import TabBarDemo from "./forms/tabbar-demo";
import PickerDemo from "./forms/picker";
import GateBanner from "./forms/gate-banner";
import TooltipDemo from "./forms/tooltip";

export class App extends React.Component<{}, {}> {

    tabs: ITabItem[] = [
        { text: "First", link: "/tabbar/first" },
        { text: "Second", link: "/tabbar/second" },
        { text: "Third", link: "/tabbar/third" },
        { text: "Fourth", link: "/tabbar/fourth" }
    ];

    leftMenu: AppLeftMenu;

    render() {
        const rightBlock = [
            <Title label="Правый блок" />,
        ];
        const centerblock = <Title label="Центральный блок" />;
        const path = hashHistory.getCurrentLocation().pathname.match(/^(\/tabbar)/);

        const tabBar = path && path[0] === "/tabbar" ? <TabBar items={this.tabs} /> : null;
        return (
            <div className="app">
                <AppBarSimple label="trusted-components" onNavMenuClick={e => { this.leftMenu.toggle(); e.stopPropagation(); } }
                    centerBlock={centerblock}
                    rightBlock={rightBlock}>
                    {tabBar}
                </AppBarSimple>
                <AppLeftMenu ref={e => this.leftMenu = (e as AppLeftMenu)}>
                    <AppLeftMenuHeader>
                        Trusted components
                    </AppLeftMenuHeader>
                    <AppLeftMenuContent>
                        <MenuItem icon="" text="Button" to="/button" />
                        <MenuItem icon="" text="Page" to="/page" />
                        <MenuItem icon="" text="Chip" to="/chip" />
                        <MenuItem icon="" text="Icon" to="/icon" />
                        <MenuItem icon="" text="Input" to="/input" />
                        <MenuItem icon="" text="List" to="/list" />
                        <MenuItem icon="" text="Table" to="/table" />
                        <MenuItem icon="" text="Modal" to="/modal" />
                        <MenuItem icon="" text="Preloader" to="/preloader" />
                        <MenuItem icon="" text="Settings" to="/settings" />
                        <MenuItem icon="" text="Pickers" to="/picker" />
                        <MenuItem icon="" text="Stepper" to="/stepper" />
                        <MenuItem icon="" text="Tree view demo" to="/treeview" />
                        <MenuItem icon="" text="Gate Banner" to="/gate-banner" />
                        <MenuItem icon="" text="Tooltip" to="/tooltip" />
                        <TreeViewContainer label="Tab bar demo" onClick={() => { console.log("Treeview click") } }>
                            <MenuItem icon="" text="TabBar 1" to="/tabbar/first" />
                            <MenuItem icon="" text="TabBar 2" to="/tabbar/second" />
                            <MenuItem icon="" text="TabBar 3" to="/tabbar/third" />
                            <MenuItem icon="" text="TabBar 4" to="/tabbar/fourth" />
                        </TreeViewContainer>
                    </AppLeftMenuContent>
                    <AppLeftMenuFooter title="Trusted components footer">
                        <a href="#">Ссылка1</a>
                        <a href="#">Ссылка2</a>
                        <a href="#">Ссылка3</a>
                    </AppLeftMenuFooter>
                </AppLeftMenu>
                <div className="content-wrapper">
                    <div className="content">
                        {this.props.children}
                        <Footer title={"Trusted-components"}>
                            <a href="#">Ссылка</a>
                            <a href="#">Ссылка</a>
                            <a href="#">Ссылка</a>
                            <a href="#">Ссылка</a>
                            <a href="#">Ссылка</a>
                        </Footer>
                    </div>
                </div>
            </div>
        );
    }

}

const router = (
    <Router history={hashHistory}>
        <Redirect from="/" to="/button" />
        <Route path="/" component={App}>
            <Route path="/button" component={Button} />
            <Route path="/page" component={Page} />
            <Route path="/chip" component={Chip} />
            <Route path="/icon" component={Icon} />
            <Route path="/input" component={Input} />
            <Route path="/list" component={List} />
            <Route path="/table" component={Table} />
            <Route path="/modal" component={Modal} />
            <Route path="/preloader" component={Preloader} />
            <Route path="/settings" component={Settings} />
            <Route path="/stepper" component={Stepper} />
            <Route path="/treeview" component={TreeViewDemo} />
            <Route path="/gate-banner" component={GateBanner} />
            <Route path="/tabbar/:name" component={TabBarDemo} />
            <Route path="/picker" component={PickerDemo} />
            <Route path="/tooltip" component={TooltipDemo} />
        </Route>
    </Router>
);

ReactDOM.render(router, document.getElementById("app"));