import * as React from "react";
import * as classnames from "classnames";

export * from "./radio";
import {ComponentColorSheme, ComponentSizeType} from "../types";
import {RandomId, getParentByClass} from "../core";
import {PhotoButton, IconButton} from "../button";
import {Recaptcha} from "../recaptcha";
import {Icon} from "../icon";

export interface IInputFieldProps {
    className?: string;
    hidden?: boolean;
    readOnly?: boolean;
    autoFocus?: boolean;
    placeholder?: string;
    error?: string;
    name?: string;
    onKeyUp?: React.KeyboardEventHandler<HTMLInputElement>;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    onBlur?: React.FocusEventHandler<HTMLInputElement>;
    onInput?: React.FocusEventHandler<HTMLInputElement>;
    onClick?: React.MouseEventHandler<HTMLInputElement>;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    label?: string;
    required?: boolean;
    description?: string;
    type?: string;
    tabIndex?: number;
    autoComplete?: boolean;

    fontSize?: "display-1" | "display-2" | "display-3" | "display-4" | "headline" | "title" | "subheading" | "body-1" | "body-2" | "caption" | "button" | "input";
    color?: string;
    colorSheme?: ComponentColorSheme;
    icon?: string;
}

export interface IInputFieldState {
    focused?: boolean;
}

export class InputField extends React.Component<IInputFieldProps, IInputFieldState> {

    static defaultProps: IInputFieldProps = {
        fontSize: "input",
        colorSheme: "light" as ComponentColorSheme
    };

    id = RandomId();

    constructor(props: IInputFieldProps) {
        super(props);
        this.state = {
            focused: false
        };

        this.onFocusHandle = this.onFocusHandle.bind(this);
        this.onBlurHandle = this.onBlurHandle.bind(this);
    }

    input: HTMLInputElement;

    onFocusHandle(e: React.FocusEvent<HTMLInputElement>) {
        this.setState({focused: true});
        this.props.onInput && this.props.onInput(e);
    }

    onBlurHandle(e: React.FocusEvent<HTMLInputElement>) {
        this.setState({focused: false});
        this.props.onBlur && this.props.onBlur(e);
    }

    render() {
        let id = this.id;

        let label: JSX.Element | null = null;
        if (this.props.label) {
            const isActive = (this.props.value || this.props.defaultValue || (this.input && this.input.value));
            const isFocus = (this.state.focused);
            const classLabel = classnames([
                (isActive ? "active" : ""),
                (isFocus ? "focus" : ""),
                (this.props.fontSize ? `font-${this.props.fontSize}` : null),
                (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
                (this.props.color ? this.props.color : null)
            ]);
            label = (
                <label className={classLabel} onClick={(e) => { e.stopPropagation(); this.input.focus(); } }>
                    {this.props.label}
                    {this.props.required ? <span className="font-red">*</span> : null}
                </label>
            );
        }

        let error: JSX.Element | null = null;
        let description: JSX.Element | null = null;
        if (this.props.error) {
            error = (
                <div className="input-error">
                    <span className="error-color">{this.props.error}</span>
                </div>
            );
        }
        else if (this.props.description) {
            description = (
                <div
                    className={"input-description " + (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null)}>
                    <span>{this.props.description}</span>
                </div>
            );
        }

        let icon: JSX.Element | null = null;
        const isFocus = (this.state.focused);
        const classIcon = classnames([
            (isFocus ? "focus" : ""),
            (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
            (this.props.description || this.props.error ? "wd" : null)]);
        if (this.props.icon) {
            icon = (
                <Icon name={this.props.icon} desc={classIcon}/>
            );
        }

        const classInput = classnames([
            (this.props.label ? "wl" : null),
            (this.props.description ? "wd" : null),
            (this.props.error ? "we" : null),
            (this.props.error ? " invalid" : ""),
            (this.props.fontSize ? `font-${this.props.fontSize}` : null),
            (this.props.colorSheme ? `sheme-${this.props.colorSheme}` : null),
            (this.props.color ? this.props.color : null)
        ]);

        const classInputField = classnames([
            (this.props.disabled ? "disabled" : null),
            (this.props.icon ? "wi" : null),
            (this.props.className || "")
        ]);

        return (
            <div className={`input-field col ${classInputField}`} hidden={this.props.hidden}>
                {icon}
                <div className="inputBox">
                    <input
                        ref={(e: any) => { this.input = e; } }
                        id={id}
                        hidden={this.props.hidden}
                        readOnly={this.props.readOnly}
                        autoFocus={this.props.autoFocus}
                        placeholder={this.props.placeholder}
                        type={this.props.type || "text"}
                        className={classInput}
                        name={this.props.name}
                        onKeyUp={this.props.onKeyUp}
                        onFocus={this.onFocusHandle}
                        onBlur={this.onBlurHandle}
                        onInput={this.props.onInput}
                        onChange={this.props.onChange}
                        onClick={this.props.onClick}
                        disabled={this.props.disabled}
                        value={this.props.value}
                        defaultValue={this.props.defaultValue}
                        tabIndex={this.props.tabIndex}
                        autoComplete={!this.props.autoComplete ? "off" : undefined}/>
                    {label}
                    {error}
                    {description}
                </div>
            </div>
        );
    }

}

export interface IInputAvatarProps {
    onChange?: React.FormEventHandler<HTMLInputElement>;
    image?: File | string;
    size?: ComponentSizeType;
    defaultImage?: string;
}

export class InputAvatar extends React.Component<IInputAvatarProps, {}> {

    static defaultProps: IInputAvatarProps = {
        size: "medium",
    };

    constructor(props: IInputAvatarProps) {
        super(props);
        this.state = {};

        this.onClick = this.onClick.bind(this);
    }

    onClick(e: React.MouseEvent<any>) {
        e.stopPropagation();
        this.input.click();
    }

    input: HTMLInputElement;

    render() {
        const {onClick} = this;
        const {onChange, image, defaultImage} = this.props;
        return (
            <div className={`input-avatar size-${this.props.size}`}
                 style={{ background: `url(${image || defaultImage || "none"}) center center / cover no-repeat,  url(/static/new/img/ava.jpg) center center / cover no-repeat` }}>
                <PhotoButton onClick={onClick} size={this.props.size} title="Сменить изображение"/>
                <input ref={(e: any) => this.input = e} hidden type="file" accept="image/*" onChange={onChange}/>
            </div>
        );
    }

}

export interface ICheckBoxProps {
    checked?: boolean;
    defaultChecked?: boolean;
    label?: string;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    disabled?: boolean;
    className?: string;
}

export class CheckBox extends React.Component<ICheckBoxProps, {}> {

    id = RandomId();

    render() {
        const classInputCheckBoxInput = classnames([
            "filled-in",
            (this.props.disabled ? "disabled" : null),
            (this.props.className || "")
        ]);
        const classInputCheckBox = classnames([
            "input-checkbox",
            (this.props.disabled ? "disabled" : null),
            (this.props.className || "")
        ]);

        return (
            <div className={classInputCheckBox}>
                <input type="checkbox" className={classInputCheckBoxInput} id={this.id}
                       defaultChecked={this.props.defaultChecked}
                       checked={this.props.checked}
                       onChange={this.props.onChange}
                       disabled={this.props.disabled}
                />
                <label htmlFor={this.id}>{this.props.label}</label>
            </div>
        );
    }

}

export interface IInputSelectProps {
    value?: string;
    defaultValue?: string;
    placeholder?: string;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    error?: string;
}

export interface IInputSelectState {
    context?: boolean;
}

export class InputSelect extends React.Component<IInputSelectProps, IInputSelectState> {

    context: HTMLElement;
    main: HTMLDivElement;

    constructor(props: IInputSelectProps) {
        super(props);
        this.state = {
            context: false
        };

        this.onClick = this.onClick.bind(this);
        this.onBlurHandler = this.onBlurHandler.bind(this);
        this.onInputHandler = this.onInputHandler.bind(this);
        this.onWindowClickHandler = this.onWindowClickHandler.bind(this);
    }

    /**
     * Выводит Context на экран
     * - Если элемент на экране, то ничего не происходит
     *
     * @protected
     *
     * @memberOf InputSelect
     */
    protected showContext(visible: boolean) {
        if (this.state.context !== visible)
            this.setState({context: visible});
    }

    protected onWindowClickHandler() {
        this.showContext(false);
    }

    onClick(e: React.MouseEvent<any>) {
        this.showContext(true);
        e.stopPropagation();
    }

    componentDidMount() {
        window.addEventListener("click", this.onWindowClickHandler);
        const modal = getParentByClass(this.refs["main"] as HTMLInputElement, "modal");
        if (modal)
            modal.addEventListener("click", this.onWindowClickHandler);
    }

    componentWillUnount() {
        window.removeEventListener("click", this.onWindowClickHandler);
        const modal = getParentByClass(this.refs["main"] as HTMLInputElement, "modal");
        if (modal)
            modal.removeEventListener("click", this.onWindowClickHandler);
    }

    protected onBlurHandler(e: React.FocusEvent<any>) {
        // TODO: Скрывать контекстное меню после потери фокуса
        // Возможно лучше скрывать после нажатия клавиши Tab
        // this.showContext(false);
    }

    protected onInputHandler(e: React.FocusEvent<any>) {
        this.showContext(true);
    }

    render() {
        return (
            <div className="input-select" ref={(e: any) => this.main = e}>
                <InputField
                    value={this.props.value}
                    defaultValue={this.props.defaultValue}
                    error={this.props.error}
                    placeholder={this.props.placeholder}
                    onChange={(e: any) => { this.showContext(true); this.props.onChange && this.props.onChange(e); } }
                    onClick={this.onClick}
                    onBlur={this.onBlurHandler}
                    onInput={this.onInputHandler}
                />
                <Icon name="arrow_drop_down"/>
                <div ref={(e: any) => this.context = e} className="input-select-context" hidden={!this.state.context}>
                    {this.props.children}
                </div>
            </div>
        );
    }

}

export interface IInputCaptchaProps {
    name?: string;
    sitekey: string;
}

export interface IInputCaptchaState {
}

export interface RecaptchaMessage<T> {
    message?: T;
    messageType: string;
}

export interface RecaptchaMessageReady extends RecaptchaMessage<any> {
    messageType: "ready_anchor";
}

export interface RecaptchaClientData {
    Ld?: any;
    Ul: number[];
}
export interface RecaptchaMessageClientData extends RecaptchaMessage<RecaptchaClientData> {
    messageType: "client_data";
}
export interface RecaptchaLoadChallenge {
    tk: {
        left: number;
        top: number;
        width: number;
        height: number;
    };
    Pi: {
        src: string;
        title: string;
    };
    Gh: string;
}
export interface RecaptchaMessageLoadChallenge extends RecaptchaMessage<RecaptchaLoadChallenge> {
    messageType: "load_challenge";
}

export interface RecaptchaShowChallenge {
    visible: boolean;
    Ld: any;
    af: {
        width: number; height: number
    };
}
export interface RecaptchaMessageShowChallenge extends RecaptchaMessage<RecaptchaShowChallenge> {
    messageType: "show_challenge";
}

export interface RecaptchaToken {
    response: string;
    Sl: number;
}
export interface RecaptchaMessageToken extends RecaptchaMessage<RecaptchaToken> {
    messageType: "token";
}

export interface RecaptchaMessageExpiry extends RecaptchaMessage<any> {
    messageType: "expiry";
}


export class InputCaptcha extends React.Component<IInputCaptchaProps, IInputCaptchaState> {

    id = RandomId();

    constructor(props: IInputCaptchaProps) {
        super(props);
        this.state = {};

        this.onMessageHandler = this.onMessageHandler.bind(this);
    }

    onMessageHandler(e: MessageEvent) {
        if (e.origin === "https://www.google.com") {
            let message: RecaptchaMessage<any> | undefined;
            try {
                message = JSON.parse(e.data);
            }
            catch (e) {
                console.error("Ошибка обработки сообщения от Google Captcha");
            }
            if (message) {
                const input = this.refs["input"] as HTMLInputElement;
                switch (message.messageType) {
                    case "token":
                        const _msg = message as RecaptchaMessageToken;
                        if (!_msg.message)
                            console.error("Reacptcha: Ошибка чтения пакета RecaptchaMessageToken");
                        else
                            input.value = _msg.message.response;
                        break;
                    case "expiry":
                        input.value = "";
                        break;
                }
            }
        }
    }

    componentDidMount() {
        window.addEventListener("message", this.onMessageHandler);
    }

    componentWillUnount() {
        window.removeEventListener("message", this.onMessageHandler);
    }

    get value(): string {
        return (this.refs["input"] as HTMLInputElement).value;
    }

    static defaultProps: IInputCaptchaProps = {
        name: "captcha",
        sitekey: "unknown"
    };

    render() {
        return (
            <div>
                <Recaptcha sitekey={this.props.sitekey} render="explicit" onLoadCallback={() => { } }/>
                <input ref="input" type="text" value="" hidden name={this.props.name}/>
            </div>
        );
    }

}