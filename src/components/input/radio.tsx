import * as React from "react";
import * as classnames from "classnames";

import * as core from "../core";

export interface InputRadioProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    className?: string;
    disabled?: boolean;
}

export class InputRadio extends React.Component<InputRadioProps, {}> {

    protected id = core.RandomId();

    render() {

        const mainClass = classnames([
            ("input-radio"),
            (this.props.disabled ? "disabled" : null),
            (this.props.className || "")
        ]);

        const inputProps = core.assign({}, this.props);

        delete inputProps.className;
        delete inputProps.label;
        inputProps.id = this.props.id || this.id;
        inputProps.type = "radio";

        return (
            <div className={mainClass}>
                <input {...inputProps} />
                {
                    this.props.label ?
                        <label htmlFor={inputProps.id}>{this.props.label}</label>
                        :
                        null
                }
            </div >
        );
    }
}