import * as React from "react";

export interface IToggleProps {
    checked?: boolean;
    defaultChecked?: boolean;
    onChange?: React.FormEventHandler<HTMLInputElement>;
    disabled?: boolean;
}

export class Toggle extends React.Component<IToggleProps, {}> {

    componentWillMount() {
        this.setState({
            checked: this.props.checked
        }, () => {
            this.render();
        });
    }

    render() {
        return (
            <div className="switch">
                <label>
                    <input
                        type="checkbox"
                        defaultChecked={this.props.defaultChecked}
                        checked={this.props.checked}
                        onChange={this.props.onChange}
                        disabled={this.props.disabled}
                        />
                    <span className="lever"></span>
                </label>
            </div>
        );
    }
}