/**
 * Объединяет несколько объектов в один
 * 
 * @export
 * @param {*} target Объект в который будут добавлены новые свойства.
 * @param {...any[]} sources Объекты, свойства которых будут добавлены в объект `target`
 * @returns Возвращает расширенный объект `target`
 */
export function assign(target: any, ...sources: any[]) {
    let res = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
        let obj = arguments[i];
        for (let prop in obj) {
            res[prop] = obj[prop];
        }
    }
    return res;
}

(Object as any).assign = (Object as any).assign || assign;