import * as moment from "moment";
import {Moment} from "moment";

export type Affix = 'am' | 'pm';

export function getFirstWeekday() {
    const sunday = moment(0).add(3, "day");
    const sundayWeekday = sunday.weekday();
    let date = sunday;
    if (sundayWeekday === 6) {
        date = sunday.add(1, "day");
    }
    return {date: date, sundayWeekday: sundayWeekday};
}

export function getUserLanguage(): string {
    return navigator.language || (navigator as any).userLanguage;
}

export function getFirstDayOfMonth(date: string): string {
    return moment(date).startOf("month").format()
}

export function getLastDayOfMonth(date: string): string {
    return moment(date).endOf("month").format()
}

export function isSameDate(date1: string| Moment, date2: string | Moment) {
    return moment(date1).startOf("day").isSame(moment(date2).startOf("day"), "date");
}

export function isBetweenDate(date: string, before: string, after: string) {
    return moment(date).isBetween(before, after);
}

export function isAfterDate(date: string, after: string) {
    return moment(date).isAfter(after);
}

export function isBeforeDate(date: string, before: string) {
    return moment(date).isBefore(before);
}

export function getAffix(time: string): Affix {
    return moment(time).hours() >= 12 ? "pm" : "am";
}

export function rad2deg(rad: number) {
    return rad * 57.29577951308232;
}