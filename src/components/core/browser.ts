declare var window: any;
declare var opr: any;
declare var InstallTrigger: any;
declare var document: any;

export class Browser {
    static isOpera(): boolean {
        return (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0;
    }
    // Firefox 1.0+
    static isFirefox(): boolean {
        return typeof InstallTrigger !== "undefined";
    }
    /**
     * At least Safari 3+: "[object HTMLElementConstructor]" 
     * 
     * @static
     * @returns {boolean}
     */
    static isSafari(): boolean {
        return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor") > 0;
    }
    /**
     * Internet Explorer 6-11
     * 
     * @static
     * @returns {boolean}
    
     */
    static isIE(): boolean {
        return /*@cc_on!@*/false || !!document.documentMode;
    }
    /**
     * Edge 20+
     * 
     * @static
     * @returns
     */
    static isEdge() {
        return !this.isIE() && !!window.StyleMedia;
    }
    /**
     * Chrome 1+
     * 
     * @static
     * @returns {boolean}
     */
    static isChrome(): boolean {
        return !!window.chrome && !!window.chrome.webstore;
    }
    // Blink engine detection
    static isBlink() {
        return (this.isChrome() || this.isOpera()) && !!window.CSS;
    }
}