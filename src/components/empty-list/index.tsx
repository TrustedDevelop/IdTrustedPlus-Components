import * as React from "react";

export interface IEmptyListProps {
    title: string;
    description: string;
    icon: string;
}

export class EmptyList extends React.Component<IEmptyListProps, {}> {
    constructor(props: IEmptyListProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="empty-info">
                <div className="content-header empty-info--header">
                    <span>{this.props.title}</span>
                </div>
                <div className="empty-info--icon">
                    <i className="material-icons">{this.props.icon}</i>
                </div>
                <div className="empty-info--description">
                    <span>{this.props.description}</span>
                </div>
            </div>
        );
    }
}
