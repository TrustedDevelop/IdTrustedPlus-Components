import * as React from "react";
import * as classnames from "classnames";

import {IconButton} from "../button";

export interface IChipProps {
    /**
     * Отображаемое имя
     * 
     * @type {string}
     * @memberOf IUserChipProps
     */
    text: string;
    /**
     * Картинка
     *  
     * @type {string}
     * @memberOf IUserChipProps
     */
    image?: string;
    /**
     * Картинка по умолчанию
     * 
     * @type {string}
     * @memberOf IChipProps
     */
    defaultImage?: string;
    /**
     * Обработчик нажатия кнопки Закрыть
     * - Если значение отсутствует, то кнопка Закрыть будет недоступна
     * 
     * @type {React.MouseEventHandler<any>}
     * @memberOf IUserChipProps
     */
    onCloseClick?: React.MouseEventHandler<any>;
}

export class Chip extends React.Component<IChipProps, {}> {
    constructor(props: IChipProps) {
        super(props);

        this.onCloseClick = this.onCloseClick.bind(this);
    }

    onCloseClick(e: React.MouseEvent<any>) {
        e.stopPropagation();
        this.props.onCloseClick && this.props.onCloseClick(e);
    }

    render() {
        const {image, text, onCloseClick, defaultImage} = this.props;

        const classLabelChip = classnames([
          "labelChip",
          (this.props.image ? "wi" : null),
          (this.props.onCloseClick ? "wc" : null)
        ]);

        return (
            <div className="chip-item animated bounceIn">
                {image ? <div className="chip-image" style={{background: `url(${image || defaultImage || "none"}) center center / cover no-repeat,  url(/static/new/img/${defaultImage || "ava.jpg"}) center center / cover no-repeat`}}/> : <div className="chip-image woi"/>}
                <div className={`${classLabelChip}`}>{this.props.text}</div>
                {onCloseClick ? <IconButton className="chip-close" icon="close" title="Закрыть" onClick={this.onCloseClick}/> : null}
            </div>
        );
    }
}