import * as React from "react";

export interface TreeViewContainerProps {
    label: string;
    collapsed?: boolean;
    disabled?: boolean;
    onClick?: Function;
}
export interface TreeViewContainerState {
    collapsed?: boolean;
}

export class TreeViewContainer extends React.Component<TreeViewContainerProps, TreeViewContainerState> {
    constructor(props: TreeViewContainerProps) {
        super(props);
        this.state = { collapsed: this.props.collapsed };
        this.toggle = this.toggle.bind(this);
        this.onContainerClick = this.onContainerClick.bind(this);
    }

    static defaultProps: TreeViewContainerProps = {
        label: "no label",
        collapsed: false,
        disabled: false
    };

    toggle() {
        this.setState({ collapsed: !this.state.collapsed });
    }

    onContainerClick(e: React.MouseEvent<HTMLDivElement>) {
        const {disabled, onClick} = this.props;
        e.stopPropagation(); !disabled && this.toggle();
        onClick && onClick();
    }



    render() {
        const {collapsed} = this.state;
        const {children, disabled, label} = this.props;
        let arrowClassName = "";
        let nodeClassName = "";
        if (!collapsed) {
            arrowClassName = "tree-view-arrow-non-collapsed";
            nodeClassName = "tree-view-node-non-collapsed";
        }
        return (
            <div className="tree-view-container" >
                <div className="tree-view-label" onClick={e => { this.onContainerClick(e) } }>
                    <span className="tree-view-label-text">{label}</span>
                    {
                        !disabled && <div className={`tree-view-arrow ${arrowClassName}`}></div>
                    }
                </div>

                <div className={`tree-view-node ${nodeClassName}`}>
                    {children}
                </div>
            </div>
        );
    }
}