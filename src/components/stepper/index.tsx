import * as React from "react";

export interface ISteppersProps { }

export class Stepper extends React.Component<ISteppersProps, {}> {

    render() {
        return (
            <div className="stepper">
                {this.props.children}
            </div>
        );
    }

}

export interface ISteppersItemProps {
    number: number;
    title: string;
}

export class StepperItem extends React.Component<ISteppersItemProps, {}> {

    render() {
        return (
            <div className="stepper-item">
                <div className="stepper-item--header">
                    <span className="stepper-item--number">{this.props.number}</span><span className="stepper-item--title">{this.props.title}</span>
                </div>
                <div className="stepper-item--content" hidden={!this.props.children}>
                    {this.props.children}
                </div>
            </div>
        );
    }

}