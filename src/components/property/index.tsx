import * as React from "react";

export interface IPropertyViewProps { }

export class PropertyView extends React.Component<IPropertyViewProps, {}> {

    render() {
        return (
            <div className="property-view">
                <div className="property-view-body">
                    {this.props.children}
                </div>
            </div>
        );
    }

}

export interface IPropertyViewItemProps {
    title: string;
    description?: string;
    value: string | number;
}

/**
 * Дочерний элемент PropertyView
 * <PropertyViewItem title value><Button/></PropertyViewItem>
 * 
 * @export
 * @class PropertyViewItem
 * @extends {React.Component<IPropertyViewItemProps, IPropertyViewItemState>}
 */
export class PropertyViewItem extends React.Component<IPropertyViewItemProps, {}> {

    render() {
        const {title, description, value, children} = this.props;
        return (
            <div className="property-view-item">
                <Property title={title} description={description}/>
                <div className="value">{value}</div>
                {
                    children ?
                        <div className="action">{children}</div>
                        :
                        null
                }
            </div>
        );
    }

}

export interface IPropertyProps {
    title: string;
    description?: string;
    className?: string;
}

export class Property extends React.Component<IPropertyProps, {}> {

    render() {
        return (
            <div className={`property ${this.props.className || "" }`}>
                <div className="title">{this.props.title}</div>
                {this.props.description ? <div className="description">{this.props.description}</div> : null}
            </div>
        );
    }

}