import * as React from "react";
import * as ReactDOM from "react-dom";
import * as classnames from "classnames";

export abstract class Portal<P, S> extends React.Component<P, S> {

    protected name: string;
    protected node: HTMLElement;

    constructor(portalName: string, className?: string) {
        super();

        const name = `portal-${portalName}`;
        let node = document.querySelector(`#${name}`) as HTMLElement;
        if (!node) {
            node = document.createElement("div");
            node.setAttribute("id", name);
            className && node.setAttribute("class", className);
            document.body.appendChild(node);
        }
        this.node = node;
        this.name = name;
    }

    abstract componentRender(): JSX.Element;

    componentDidUpdate() {
        ReactDOM.render((
            this.componentRender()
        ), this.node);
    }

    render() {
        return null;
    }

}