import * as React from "react";

// Components
import {Property} from "../property";

export interface ISettingsProps { }

export class Settings extends React.Component<ISettingsProps, {}> {

    render() {
        return (
            <div className="settings">
                {this.props.children}
            </div>
        );
    }

}

export interface ISettingItemProps {
    name: string;
    description?: string;
}

export class SettingItem extends React.Component<ISettingItemProps, {}> {

    render() {
        return (
            <div>
                <div className="setting-item">
                    <div>
                        <Property title={this.props.name} description={this.props.description}/>
                    </div>
                    <div className="setting-item-control">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }

}