import * as React from "react";
import * as classnames from "classnames";

import { ComponentBase, ComponentColor, ComponentBgColor, ComponentSize } from "../types";

export interface IHeaderProps extends ComponentBase, ComponentSize, ComponentColor, ComponentBgColor {
    bgImage?: string;
}

export class PageHeader extends React.Component<IHeaderProps, {}> {

    static defaultProps: IHeaderProps = {
        color: "black",
        bgImage: "",
        bgColor: "white",
        size: "small",
        className: ""
    };

    render() {

        const cn = classnames([
            "page--header",
            this.props.className,
            `size-${this.props.size}`,
            this.props.color,
            `bg-${this.props.bgColor}`
        ]);

        return (
            <div
                className={cn}
                style={this.props.bgImage ? { backgroundImage: `url(${this.props.bgImage})` } : undefined}
                >
                {this.props.children}
            </div>
        );
    }

}

export interface IContentProps extends ComponentBase, ComponentColor, ComponentBgColor {
}

export class PageContent extends React.Component<IContentProps, {}> {

    static defaultProps: IContentProps = {
        className: ""
    };

    render() {

        let mainClass = classnames([
            "page--content",
            this.props.className,
            this.props.color,
            `bg-${this.props.bgColor}`
        ]);

        return (
            <div className={mainClass}>
                {this.props.children}
            </div>
        );
    }

}

export interface IContentProps extends ComponentBase, ComponentColor, ComponentBgColor {
}

export class PageFooter extends React.Component<IContentProps, {}> {

    static defaultProps: IContentProps = {
        className: ""
    };

    render() {

        let mainClass = classnames([
            "page--footer",
            this.props.className,
            this.props.color,
            `bg-${this.props.bgColor}`
        ]);

        return (
            <div className={mainClass}>
                {this.props.children}
            </div>
        );
    }

}

export interface IPageProps extends ComponentBase {
    title?: string;
}

export class Page extends React.Component<IPageProps, {}> {

    static defaultProps: IPageProps = {
        className: ""
    };

    render() {

        let mainClass = classnames([
            "page",
            this.props.className
        ]);

        return (
            <div className={mainClass}>
                {this.props.children}
            </div>
        );
    }

}