import * as React from "react";
import * as classnames from "classnames";

import { Page, PageHeader, PageContent } from "./page";
import { Title } from "../text";
import { ComponentBase, ComponentTitle } from "../types";

export interface IPageSimpleProps extends ComponentBase, ComponentTitle { }

export class PageSimple extends React.Component<IPageSimpleProps, {}> {

    render() {

        const name = classnames(["page-simple"]);

        return (
            <Page className={name}>
                <PageHeader>
                    <Title label={this.props.label} description={this.props.description} />
                </PageHeader>
                <PageContent>
                    {this.props.children}
                </PageContent>
            </Page>
        );
    }

}