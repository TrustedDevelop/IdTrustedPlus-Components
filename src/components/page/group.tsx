import * as React from "react";

export interface IPageGroupProps {
    /**
     * Тип группировки
     * - row: группировка по ряду. По умолчанию 
     * - col: группировка по колонке
     * 
     * @type {("row" | "col")}
     */
    type?: "row" | "col";
    className?: string;
}

export class PageGroup extends React.Component<IPageGroupProps, {}> {

    render() {
        const {type, className} = this.props;
        return (
            <div className={`page-group ${type || "row"} ${className ? className : ""}`}>
                {this.props.children}
            </div>
        );
    }

}