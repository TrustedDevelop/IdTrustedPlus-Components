import * as React from "react";
import * as classnames from "classnames";

import * as core from "../core";


import { ComponentBase, ComponentSize, ComponentColor } from "../types";

export interface IPreloaderProps extends ComponentBase, ComponentSize, ComponentColor {
  hidden?: boolean;
}

export interface IPreloaderState {
  hide: boolean;
  show: boolean;
}

/**
 * Круглый preloader
 * Возожные размеры: small | medium | large
 * 
 * @export
 * @class PreloaderCircle
 * @extends {React.Component<IPreloaderProps, IPreloaderState>}
 */
export class PreloaderCircle extends React.Component<IPreloaderProps, IPreloaderState> {

  static defaultProps: IPreloaderProps = {
    size: "medium",
    color: "blue"
  };

  constructor(props: IPreloaderProps) {
    super(props);
    this.state = {
      hide: false,
      show: true
    };
  }

  render() {
    const mainClass = classnames([
      ("preloader-wrapper active"),
      (this.props.size),
      (this.props.className || "")
    ]);

    return (
      <div className={mainClass}>
        <div className={`spinner-layer border-${this.props.color}`}>
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>
      </div>
    );
  }
}

/**
 * Линейный preloader
 * Есть 2 типа: 
 * - determinate - ширина устанавливается через {width: 70%}
 * - indeterminate - "бегущая" полоска
 * Обоим типам можно задать className, color, style, type
 * 
 * @export
 * @class PreloaderLine
 * @extends {React.Component<IPreloaderProps, IPreloaderState>}
 */

export type PreloaderLineType = "determinate" | "indeterminate";
export interface IPreloaderLineProps {
  type?: PreloaderLineType;
  className?: string;
  color?: ComponentColor | string;
  style?: any;
}

export class PreloaderLine extends React.Component<IPreloaderLineProps, IPreloaderState> {
  static defaultProps: IPreloaderLineProps = {
    color: "blue" as ComponentColor,
    type: "indeterminate"
  };


  constructor(props: IPreloaderProps) {
    super(props as any);
    this.state = {
      hide: false,
      show: true
    };
  }

  render() {
    const mainClass = classnames([
      ("progress-line"),
      (this.props.className || "")
    ]);

    if (this.props.style && this.props.type === "indeterminate") {
      delete this.props.style.width;
    }

    return (
      <div className={mainClass}>
        <div className={`progress bg-${this.props.color}`}></div>
        <div className={`${this.props.type} bg-${this.props.color}`} style={this.props.style}></div>
      </div>
    );
  }
}



/**
 * Круглый preloader
 * Возожные размеры: small | medium | large
 * Меняется цвет preloader blue-red-yellow-green
 * 
 * @export
 * @class PreloaderFlashingColors
 * @extends {React.Component<IPreloaderProps, IPreloaderState>}
 */
export class PreloaderFlashingColors extends React.Component<IPreloaderProps, IPreloaderState> {

  static defaultProps: IPreloaderProps = {
    size: "medium"
  };

  constructor(props: IPreloaderProps) {
    super(props);
    this.state = {
      hide: false,
      show: true
    };
  }

  render() {
    const mainClass = classnames([
      ("preloader-wrapper active"),
      (this.props.size),
      (this.props.className || "")
    ]);

    return (
      <div className={mainClass}>
        <div className="spinner-layer spinner-blue">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>

        <div className="spinner-layer spinner-red">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>

        <div className="spinner-layer spinner-yellow">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>

        <div className="spinner-layer spinner-green">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>
      </div>
    );
  }
}
