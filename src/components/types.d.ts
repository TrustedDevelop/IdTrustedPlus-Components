declare type ComponentSizeType = "small" | "medium" | "large";
declare type ComponentFontSizeType = "display-1" | "display-2" | "display-3" | "display-4" | "headline" | "title" | "subheading" | "body-1" | "body-2" | "caption" | "button";

interface ComponentBase {
    className?: string;
}

interface ComponentSize {
    size?: ComponentSizeType;
}

interface ComponentFontSize {
    fontSize?: ComponentFontSizeType;
}

interface ComponentColor {
    color?: string;
}

interface ComponentColorSheme {
    colorSheme?: string;
}

interface ComponentBgColor {
    bgColor?: string;
}

interface ComponentTitle {
    label: string;
    description?: string;
}

interface ComponentTooltip {
    label: string;
}