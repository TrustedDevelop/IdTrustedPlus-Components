import * as React from "react";
import * as classnames from "classnames";

import {ComponentBase, TooltipPosition} from "../types";
import { hashHistory, Link } from "react-router";
import { ContextMenu } from "../menu";
import { Icon } from "../icon";
import { MoreButton, IconButton } from "../button";

export interface IListProps extends ComponentBase { }

export class List extends React.Component<IListProps, {}> {

    render() {
        const {children, className} = this.props;
        const classname = classnames(["list", className]);

        return (
            <div className={classname}>
                {children}
            </div>
        );
    }

}

export interface IListHeaderProps extends ComponentBase { }

export class ListHeader extends React.Component<IListHeaderProps, {}> {

    elements: JSX.Element[] = [];

    render() {
        this.elements = [];
        const children = this.props.children as any[];
        const className = this.props.className;
        const classname = classnames(["list-item", className]);
        return (
            <div className="header">
                <div className={classname}>
                    {children}
                </div>
            </div>
        );
    }

}

export type ListItemType = "icon" | "name" | "cell" | "menu";
export type ListItemSort = "less" | "more";

export interface IListHeaderItemProps {
    type: ListItemType;
    text: string;
    sort?: ListItemSort;
    onClick?: React.MouseEventHandler<any>;
    className?: string;
}

export class ListHeaderItem extends React.Component<IListHeaderItemProps, {}> {

    render() {
        const item_type = `list-item--${this.props.type} ${this.props.className || ""}`;
        const sort = this.props.sort ? <i className="material-icons">{`expand_${this.props.sort}`}</i> : null;
        // expand_less expand_more
        return (
            <div className={item_type} onClick={this.props.onClick}>
                <span>{this.props.text}</span>
                {sort}
            </div>
        );
    }

}

export interface IListHeaderCustomItemProps {
    type: ListItemType;
    className?: string;
}

export class ListHeaderCustomItem extends React.Component<IListHeaderCustomItemProps, {}> {

    constructor(props: IListHeaderCustomItemProps) {
        super(props);
        this.state = {};
    }

    render() {
        const item_type = `list-item--${this.props.type} ${this.props.className || ""}`;
        return (
            <div className={item_type} >
                {this.props.children}
            </div>
        );
    }
}

export interface IListItemProps {
    onClick?: React.MouseEventHandler<any>;
    to?: string;
    tooltip?: string;
    tooltipPosition?: TooltipPosition;
}

export class ListItem extends React.Component<IListItemProps, {}> {

    constructor(props: IListItemProps) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e: React.MouseEvent<any>) {
        const {to, onClick} = this.props;

        if (onClick) {
            onClick(e);
        }
        else if (to) {
            hashHistory.push(to);
        }
    }

    render() {
        const {to, onClick, tooltip, tooltipPosition} = this.props;

        const clickable = to || onClick ? " clickable" : "";
        const className = `list-item ${clickable}`;

        const rootProps: any = {};
        if (tooltip) {
            rootProps["data-tooltip"] = tooltip;
            tooltipPosition && (rootProps["data-tooltip-position"] = tooltipPosition);
        }

        rootProps["className"] = className;

        if (to)
            return (
                <Link to={to} {...rootProps}>
                    {this.props.children}
                </Link>
            );
        else
            return (
                <div onClick={this.onClick} {...rootProps}>
                    {this.props.children}
                </div>
            );
    }

}

export interface IListCustomProps {
    type: ListItemType;
    className?: string;
}

export class ListCustom extends React.Component<IListCustomProps, {}> {

    render() {
        return (
            <div className={`list-item--${this.props.type}  ${this.props.className || ""}`}>
                {this.props.children}
            </div>
        );
    }

}

export interface IListIconProps {
    icon: string;
    color?: string;
}

export class ListIcon extends React.Component<IListIconProps, {}> {

    render() {
        return (
            <div className="list-item--icon">
                <i className={`material-icons circle ${this.props.color}`}>{this.props.icon}</i>
            </div>
        );
    }

}


export interface IListIconTextProps {
    text: string;
    color?: string;
}

export class ListIconText extends React.Component<IListIconTextProps, {}> {

    render() {
        return (
            <div className={"list-item--icon circle " + this.props.color}>
                <i className="list-item-text">{this.props.text}</i>
            </div >
        );
    }

}

export interface IListImageProps {
    image: string;
    defaultImage?: string;
}

export class ListImage extends React.Component<IListImageProps, {}> {

    render() {
        const {image, defaultImage} = this.props;
        return (
            <div className="list-item--image" style={{
                background: `url(${image || defaultImage || "none"}) center center / cover no-repeat,  url(/static/new/img/ava.jpg) center center / cover no-repeat`
            }}></div>
        );
    }

}

export interface IListCellProps {
    className?: string;
    text?: string;
}

export class ListCell extends React.Component<IListCellProps, {}> {

    render() {
        return (
            <div className={`list-item--cell ${this.props.className}`}>{this.props.text || this.props.children}</div>
        );
    }

}

export interface IListNameProps {
    label: string;
    description?: string;
    subDescription?: string;
}

export class ListName extends React.Component<IListNameProps, {}> {

    constructor(props: IListNameProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="list-item--name">
                <div className="label">{this.props.label}</div>
                {this.props.description ? <div className="description">{this.props.description}</div> : null}
                {this.props.subDescription ? <div className="description">{this.props.subDescription}</div> : null}
            </div>
        );
    }

}

export interface IListMenuProps { }

export class ListMenu extends React.Component<IListMenuProps, {}> {

    constructor(props: IListMenuProps) {
        super(props);
        this.state = {};

        this.onClick = this.onClick.bind(this);
    }

    protected menu: any;

    onClick(e: React.MouseEvent<any>) {
        this.menu.click(e);
    }

    render() {
        return (
            <div className="list-item--menu" onClick={this.onClick}>
                <Icon name="more_vert" />
                <ContextMenu ref={e => this.menu = e}>
                    {this.props.children}
                </ContextMenu>
            </div>
        );
    }


}



export interface IListActionProps {
    icon: string;
    color?: string;
    onClick: React.MouseEventHandler<any>;
}

export class ListAction extends React.Component<IListActionProps, {}> {

    constructor(props: IListActionProps) {
        super(props);
        this.state = {};

        this.onClick = this.onClick.bind(this);
    }

    protected menu: any;

    onClick(e: React.MouseEvent<any>) {
        this.menu.click(e);
    }

    render() {
        return (
            <div className="list-item--action">
                <IconButton icon={this.props.icon} color={this.props.color} onClick={this.props.onClick}/>
            </div>
        );
    }


}

export interface IListMoreProps {
    /**
     * Обработчик события нажатия по кнопке
     *
     * @type {React.MouseEventHandler<any>}
     * @memberOf IListMoreProps
     */
    onClick?: React.MouseEventHandler<any>;
    /**
     * Задает свойство видимости объекта
     *
     * @type {boolean}
     * @memberOf IListMoreProps
     */
    hidden?: boolean;
}

/**
 * Отображает юблок с кнопкой подгрузки списка
 *
 * @export
 * @class ListMore
 * @extends {React.Component<IListMoreProps, IListMoreState>}
 */
export class ListMore extends React.Component<IListMoreProps, {}> {

    render() {
        return (
            <div className={`list-more ${this.props.hidden ? "hidden" : ""}`} >
                <MoreButton onClick={this.props.onClick} />
            </div>
        );
    }

}