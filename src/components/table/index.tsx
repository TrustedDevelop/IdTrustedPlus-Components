import * as React from "react";


//Осмыслить и разобраться
interface ITableItem {
    render(): JSX.Element;
}

class TItem2 implements ITableItem {
    render() {
        return <div />
    }
}
class TItem implements ITableItem {

    name: string;
    descr: string;
    image: string;

    constructor(o: any) {

    }

    render() {
        return (
            <tr>
                <td>{this.name}</td>
                <td>{this.descr}</td>
                <td><img src={this.image} /></td>
            </tr>
        )
    }
}

//Рабочая версия

export interface ITableProps { }

export class Table extends React.Component<ITableProps, {}> {

    cell123 = "Test";

    tableHeadArray: string[] = ["11", "12", "13"];

    tableArray: any[][] = [
        [31, 32, 33],
        [21, 22, 23, 24, 25, 26, 27],
        [41, 42, 43],
        [11, 12, 13],
    ];

    tableFooterArray: string[] = ["11", "12", "13"];


    render() {

        const tableArray = this.tableArray.sort();

        const rows = this.tableArray.map(row => (
            <tr>
                {row.map(item => <td>{item}</td>)}
            </tr>
        ));

        // let tableRow = [];
        // for (const row of this.tableArray) {
        //     let tableCell = [];
        //     for (const cell in row) {
        //         tableCell.push(<td> {row[cell]} </td>);
        //     }
        //     tableRow.push(<tr> {tableCell} </tr>);
        // };

        let tableRow: JSX.Element[] = [];
        for (const row of this.tableArray) {
            let tableCell: JSX.Element[] = [];
            for (const cell in row) {
                tableCell.push(<td> {row[cell]} </td>);
            }
            tableRow.push(<tr> {tableCell} </tr>);
        };


        let cont = "NNENENE";
        let test = <div> ${cont} </div>;

        return (
            <div className="table">
                <table>
                    <thead>
                        <tr><td>thead1</td><td>thead2</td></tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                    <tfoot>
                        <tr><td>tfoot1</td><td>tfoot2</td></tr>
                    </tfoot>
                </table>
            </div>
        );
    }
}