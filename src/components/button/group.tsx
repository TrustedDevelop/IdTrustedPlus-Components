import * as React from "react";
import * as classname from "classnames";

import { ComponentBase } from "../types";

export interface IButtonGroupProps extends ComponentBase {
    position?: "top" | "bottom";
    align?: "left" | "center" | "right";
}

export class ButtonGroup extends React.Component<IButtonGroupProps, {}> {

    static defaultProps: IButtonGroupProps = {
    };

    render() {
        const props = this.props;

        const names = ["btn-group", props.className, this.props.position ? `pos-${this.props.position}` : null, this.props.align ? `align-${this.props.align}` : null];
        const name = classname(names);

        return (
            <div className={`${name}`}>
                {this.props.children}
                <div className="clear"></div>
            </div>
        );
    }
}