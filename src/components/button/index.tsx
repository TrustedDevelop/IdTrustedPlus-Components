import * as React from "react";
import {Link} from "react-router";

import {ComponentBase, ComponentSizeType, TooltipPosition} from "../types";
import * as core from "../core";
import * as copy from "copy-to-clipboard";
import {IconEdit, IconMore, IconRefresh, IconPhoto, IconAdd, Icon} from "../icon";

export * from "./group";

type BaseButtonPosition = "left" | "center" | "right";

export interface IBaseButtonProps {
    color?: string;
    title?: string;
    bgColor?: string;
    onClick?: React.MouseEventHandler<any>;
    disabled?: boolean;
    className?: string;
    to?: string;
    hidden?: boolean;
    href?: string;
    target?: "_blank" | "_self" | "_parent" | "_top";
    position?: BaseButtonPosition;
    tooltip?: string;
    tooltipPosition?: TooltipPosition;
}

class BaseButton extends React.Component<IBaseButtonProps, {}> {

    constructor(props: IBaseButtonProps) {
        super(props);
        this.state = {};
    }

    static defaultProps: IBaseButtonProps = {
        color: "black",
        bgColor: "white"
    };

    render() {
        if (this.props.hidden)
            return null;

        let className = ["btn"];

        if (this.props.disabled)
            className.push("disabled");

        if (this.props.className)
            className.push(this.props.className);

        const {position, tooltip, tooltipPosition} = this.props;

        const rootProps: any = {};
        position && (rootProps["className"] = `pos-${position}`);

        if (this.props.tooltip) {
            rootProps["data-tooltip"] = tooltip;
            rootProps["data-tooltip-position"] = tooltipPosition ? tooltipPosition : "top";
        }

        return (
            <div className={this.props.position ? `pos-${this.props.position}` : undefined} {...rootProps}>
                {
                    !this.props.disabled && this.props.to ?
                        <Link
                            className={className.join(" ")}
                            to={this.props.to}
                            onClick={this.props.onClick}
                            title={this.props.title}
                        >
                            <div className="background"></div>
                            <div className="content">
                                {this.props.children}
                            </div>
                        </Link>
                        :
                        <a
                            className={className.join(" ")}
                            onClick={!this.props.disabled ? this.props.onClick : undefined}
                            href={!this.props.disabled ? this.props.href : undefined}
                            title={this.props.title}
                            target={this.props.target}>
                            <div className={`background bg-${this.props.bgColor}`}></div>
                            <div className={`content ${this.props.color}`}>
                                {this.props.children}
                            </div>
                        </a>
                }
            </div>
        );
    }

}

export interface IFlatButtonProps extends IBaseButtonProps {
}

export class FlatButton extends React.Component<IFlatButtonProps, {}> {

    static defaultProps: IRaisedButtonProps = {
        color: "black",
        className: ""
    };

    render() {
        let props = core.assign({}, this.props);
        props.bgColor = props.color;
        props.className = `flat ${props.className}`;

        return (
            <BaseButton {...props} />
        );
    }

}

export interface IRaisedButtonProps extends IBaseButtonProps {
}

export class RaisedButton extends React.Component<IRaisedButtonProps, {}> {

    static defaultProps: IRaisedButtonProps = {
        bgColor: "blue",
        color: "white",
        className: ""
    };

    render() {
        let props = core.assign({}, this.props);
        props.className = `raised ${props.className}`;

        return (
            <BaseButton {...props} />
        );
    }

}

export interface IRoundButtonProps extends IBaseButtonProps {
    size?: ComponentSizeType;
}

export class RoundButton extends React.Component<IRoundButtonProps, {}> {

    static defaultProps: IRoundButtonProps = {
        size: "medium",
        color: "black",
        className: ""
    };

    render() {
        let props = core.assign({}, this.props);
        props.bgColor = props.color;
        props.className = `round ${this.props.size} ${props.className}`;

        return (
            <BaseButton {...props} />
        );
    }

}

export interface IIconButtonBaseProps extends IRoundButtonProps {
}

export interface IIconButtonProps extends IIconButtonBaseProps {
    icon: string;
}

export class IconButton extends React.Component<IIconButtonProps, {}> {

    static defaultProps: IIconButtonProps = {
        icon: "",
        size: "small",
        className: ""
    };

    render() {
        let props = core.assign({}, this.props);
        props.className = `icon ${props.className}`;

        return (
            <RoundButton {...props}>
                <Icon name={this.props.icon}/>
            </RoundButton>
        );
    }

}

export interface ICopyButtonProps extends IIconButtonBaseProps {
    text: string;
}

export class CopyButton extends React.Component<ICopyButtonProps, {}> {

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-copy ${props.className}`;
        props.icon = `assignment`;
        props.onClick = (e: any) => {
            if (!this.props.disabled) {
                this.props.onClick && this.props.onClick(e);
                copy(this.props.text);
            }
        };

        return (
            <IconButton {...props} />
        );
    }

}

export interface IEditButtonProps extends IIconButtonBaseProps {
}

export class EditButton extends React.Component<IEditButtonProps, {}> {

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-edit ${props.className}`;
        props.icon = `mode_edit`;

        return (
            <IconButton {...props} />
        );
    }

}

export class RefreshButton extends React.Component<IRoundButtonProps, {}> {

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-refresh ${props.className}`;
        props.icon = `refresh`;

        return (
            <IconButton {...props} />
        );
    }

}

export class MoreButton extends React.Component<IRoundButtonProps, {}> {

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-more ${props.className}`;
        props.icon = `expand_more`;

        return (
            <IconButton {...props} />
        );
    }

}


export interface IPhotoButtonProps extends IIconButtonBaseProps {
}

export class PhotoButton extends React.Component<IPhotoButtonProps, {}> {

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-photo ${props.className}`;
        props.icon = `photo_camera`;

        return (
            <IconButton {...props} />
        );
    }

}

export interface IAddButtonProps extends IIconButtonBaseProps {
}

export class AddButton extends React.Component<IAddButtonProps, {}> {

    static defaultProps = {
        color: "green",
        title: "Добавить"
    };

    render() {
        let props = core.assign({}, this.props);
        props.className = `btn-photo ${props.className}`;
        props.icon = `add`;

        return (
            <IconButton {...props} />
        );
    }
}

export interface IActionButtonProps extends IRoundButtonProps {
}

export class ActionButton extends React.Component<IActionButtonProps, {}> {

    static defaultProps = {
        color: "red",
        size: "medium"
    };

    render() {
        const props = core.assign({}, this.props);
        props.className = `btn-act ${props.className}`;
        return (
            <RoundButton {...props} />
        );
    }

}

export interface IFixedCreateProps extends IRoundButtonProps {
}

export class FixedCreate extends React.Component<IFixedCreateProps, {}> {

    render() {
        return (
            <ActionButton {...this.props}><IconAdd /></ActionButton>
        );
    }

}