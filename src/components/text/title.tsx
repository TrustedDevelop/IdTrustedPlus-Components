import * as React from "react";
import * as classnames from "classnames";

import { ComponentBase, ComponentTitle, ComponentFontSize, ComponentColorSheme, ComponentColor } from "../types";

export interface ITitleProps extends ComponentFontSize, ComponentBase, ComponentTitle, ComponentColorSheme, ComponentColor {
}

export class Title extends React.Component<ITitleProps, {}> {

    static defaultProps: ITitleProps = {
        label: "",
        fontSize: "title",
        colorSheme: "dark",
    };

    render() {

        const name1 = classnames(["title", this.props.className]);
        const name2 = classnames(["title--label", `font-${this.props.fontSize}`, `sheme-${this.props.colorSheme}`, this.props.color]);
        const name3 = classnames(["title--description", "font-subheading", `sheme-${this.props.colorSheme}`, this.props.color]);

        return (
            <div className={name1}>
                <div className={name2}>{this.props.label}</div>
                {
                    this.props.description ?
                        <div className={name3}>{this.props.description}</div>
                        :
                        null
                }
            </div>
        );
    }
}

export interface ITextProps extends ComponentFontSize, ComponentBase, ComponentColorSheme, ComponentColor {
}

export class Text extends React.Component<ITextProps, {}> {

    static defaultProps: ITextProps = {
        fontSize: "body-1",
        colorSheme: "dark",
    };

    render() {

        const name = classnames(["text", `font-${this.props.fontSize}`, `sheme-${this.props.colorSheme}`, this.props.color]);

        return (
            <div className={name}>
                {this.props.children}
            </div>
        );
    }
}