import * as React from "react";
import * as classnames from "classnames";
import {RandomId} from "../core";

declare const grecaptcha: any;

export interface RecaptchaProps {
    sitekey: string;
    className?: string;
    render?: "explicit" | "onload";
    theme?: string;
    type?: string;
    size?: string;
    id?: string;
    tabindex?: string;
    loadCallbackName?: string;
    verifyCallbackName?: string;
    expiredCallbackName?: string;
    onLoadCallback?: Function;
    onVerifyCallback?: Function;
    onExpiredCallback?: Function;
}

export class Recaptcha extends React.Component<RecaptchaProps, {}> {
    static defaultProps: RecaptchaProps = {
        sitekey: "",
        className: "",
        loadCallbackName: "onloadCallback",
        verifyCallbackName: "verifyCallback",
        expiredCallbackName: "expiredCallback",
        render: "onload",
        theme: "light",
        type: "image",
        size: "normal",
        tabindex: "0",
    };

    constructor(props: RecaptchaProps) {
        super();
        this.elementId = props.id || RandomId();
    }

    elementId: string;
    id: number;

    componentDidMount() {
        this.id = grecaptcha.render(this.elementId, {
            sitekey: this.props.sitekey,
            callback: (this.props.onVerifyCallback) ? this.props.onVerifyCallback : undefined,
            theme: this.props.theme,
            type: this.props.type,
            size: this.props.size,
            tabindex: this.props.tabindex,
            "expired-callback": (this.props.onExpiredCallback) ? this.props.onExpiredCallback : undefined,
        });

        this.props.onLoadCallback && this.props.onLoadCallback();
    }

    reset(id?: number) {
        return grecaptcha.reset(id || (window as any).___grecaptcha_cfg.count - 1);
    }

    execute(id?: number) {
        return grecaptcha.execute(id || (window as any).___grecaptcha_cfg.count - 1);
    }

    render() {
        if (this.props.render === "explicit" && this.props.onLoadCallback) {
            return <div id={this.elementId}/>
            // return (
            //     <div id={this.id}
            //         data-onloadcallbackname={this.props.loadCallbackName}
            //         data-verifycallbackname={this.props.verifyCallbackName}
            //         ></div>
            // );
        }

        return (
            <div className={classnames('g-recaptcha', this.props.className)}
                 data-sitekey={this.props.sitekey}
                 data-theme={this.props.theme}
                 data-type={this.props.type}
                 data-size={this.props.size}
                 data-tabindex={this.props.tabindex}
            ></div>
        );
    }
}
