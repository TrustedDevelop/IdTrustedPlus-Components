import * as React from "react";
import * as ReactDOM from "react-dom";
import * as classnames from "classnames";

import { ComponentBase } from "../types";
import { Portal } from "../portal";
import { ANIMATION_HIDE_DELAY } from "./config";
import { Title } from "../text";

export interface IModalProps extends ComponentBase {
    onHide?: () => void;
}

export interface IModalState {
    show?: boolean;
    hidden?: boolean;
}

export class Modal extends Portal<IModalProps, IModalState> {

    protected node: HTMLElement;

    constructor(props: IModalProps) {
        super("modal");
        this.state = {
            show: false,
            hidden: true
        };

        this.onBackgroundClick = this.onBackgroundClick.bind(this);
        this.onModalClick = this.onModalClick.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
    }

    componentDidMount() {
        window.addEventListener("keyup", this.onKeyUp as any);
    }

    componentWillUnmount() {
        window.removeEventListener("keyup", this.onKeyUp as any);
    }

    /**
     * Отобразить модальное окно 
     */
    show() {
        // document.body.style.overflow = "hidden"; // Удаляет скроллы главного окна
        this.setState({
            hidden: false
        }, () => {
            setTimeout(() => {
                this.setState({
                    show: true
                });
            }, 100);
        });
    }

    /**
     * Скрыть модальное окно
     */
    hide() {
        // document.body.style.overflow = ""; // Добавляет скроллы главного окна
        this.setState({
            show: false
        }, () => {
            // Скрыть элемент после анимации
            setTimeout(() => {
                this.setState({
                    hidden: true
                }, this.props.onHide);
            }, ANIMATION_HIDE_DELAY);
        });
    }

    onBackgroundClick(e: React.MouseEvent<HTMLDivElement>) {
        this.hide();
    }

    onModalClick(e: React.MouseEvent<HTMLDivElement>) {
        e.stopPropagation();
    }

    onKeyUp(e: React.KeyboardEvent<HTMLInputElement>) {
        e.stopPropagation();
        if (e.keyCode === 27) { // ESCAPE
            this.hide();
        }
    }

    componentRender() {
        let modalBgClass = classnames([
            "modal-background",
            this.state.show ? "show" : null
        ]);
        let modalClass = classnames([
            "modal",
            this.props.className
        ]);

        return (
            <div className={modalBgClass} hidden={this.state.hidden}
                onClick={this.onBackgroundClick}>
                <div className={modalClass} onClick={this.onModalClick}>
                    {!this.state.hidden ? this.props.children : null}
                </div>
            </div>
        );
    }

}