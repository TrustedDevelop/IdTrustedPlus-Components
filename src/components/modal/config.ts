export const ANIMATION_HIDE_DELAY = 500;
export const MODAL_TITLE_INFO = "Информация";
export const MODAL_TITLE_ERROR = "Ошика";
export const MODAL_TITLE_WARNING = "Предупреждение";
export const MODAL_TITLE_QUESTION = "Вопрос";