import * as React from "react";
import * as classnames from "classnames";

import { ComponentBase } from "../types";
import { MODAL_TITLE_INFO, MODAL_TITLE_ERROR, MODAL_TITLE_QUESTION, MODAL_TITLE_WARNING } from "./config";
import { Modal } from "./base";
import { Title, Text } from "../text";
import { Page, PageHeader, PageContent, PageFooter } from "../page";
import { RaisedButton, FlatButton, ButtonGroup } from "../button";
import { Icon } from "../icon";
import { assign } from "../core";

enum ModalMessageButtons {
    cancel = 1,
    ok = 2,
    yes = 4,
    no = 8,
}

export type ModalMessageType = "info" | "error" | "warn" | "question";

export interface IModalMessageProps extends ComponentBase { }

export interface IModalMessageState extends ComponentBase {
    label: string;
    message: string;
    type: ModalMessageType;
    className: string;
    /**
     * Обработчик закрытия
     * 
     * @memberOf IModalMessageProps
     */
    onResult: (result: boolean) => void;
}

export class ModalMessage extends React.Component<IModalMessageProps, IModalMessageState> {

    static defaultState: IModalMessageState = {
        type: "info",
        message: "",
        label: "",
        onResult: () => { },
        className: ""
    };

    protected modal: Modal;

    constructor(props: IModalMessageProps) {
        super(props);
        this.state = ModalMessage.defaultState;
    }

    show(params: IModalMessageState) {
        const _params: IModalMessageState = assign({}, ModalMessage.defaultState, params);
        this.setState(_params);
        this.modal.show();
    }

    hide() {
        this.modal.hide();
    }

    render() {
        let buttons = ModalMessageButtons.ok;
        let title = this.state.label || MODAL_TITLE_INFO;
        let icon = "info";
        let color = "";
        let bgColor = "";

        switch (this.state.type) {
            case "question":
                title = this.state.label || MODAL_TITLE_QUESTION;
                buttons = ModalMessageButtons.yes | ModalMessageButtons.no;
                icon = "help";
                break;
            case "warn":
                title = this.state.label || MODAL_TITLE_WARNING;
                buttons = ModalMessageButtons.yes | ModalMessageButtons.no;
                icon = "warning";
                break;
            case "error":
                title = this.state.label || MODAL_TITLE_ERROR;
                icon = "error";
                color = "white";
                bgColor = "error-color";
                break;
            case "info":
            default:
        }

        const onResult = this.state.onResult || function() { };

        const mainClass = classnames([
            "modal-message",
            this.props.className,
        ]);

        return (
            <Modal className={mainClass} ref={(e: any) => this.modal = e}>
                <Page>
                    <PageHeader bgColor={bgColor}>
                        <Title label={title} color={color}></Title>
                    </PageHeader>
                    <PageContent>
                        <Text>{this.state.message}</Text>
                    </PageContent>
                    <PageFooter>
                        <ButtonGroup align="right">
                            {buttons & ModalMessageButtons.cancel ? <FlatButton onClick={() => this.hide() || onResult(false)}>Отмена</FlatButton> : null}
                            {buttons & ModalMessageButtons.ok ? <RaisedButton onClick={() => this.hide() || onResult(true)}>OK</RaisedButton> : null}
                            {buttons & ModalMessageButtons.no ? <FlatButton onClick={() => this.hide() || onResult(false)}>Нет</FlatButton> : null}
                            {buttons & ModalMessageButtons.yes ? <RaisedButton onClick={() => this.hide() || onResult(true)}>Да</RaisedButton> : null}
                        </ButtonGroup>
                    </PageFooter>
                </Page>
            </Modal>
        );
    }

}