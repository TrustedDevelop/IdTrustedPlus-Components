import * as React from "react";
import * as classnames from "classnames";

export interface IIconProps {
    name: string;
    color?: string;
    desc?: string;
}

export class Icon extends React.Component<IIconProps, {}> {

    constructor(props: IIconProps) {
        super(props);
        this.state = {};
    }

    static defaultProps: IIconProps = {
        name: "",
    };

    render() {
        const {name, color, desc} = this.props;
        const className = classnames("material-icons", color, desc);
        return (
            <i className={className}>{name}</i>
        );
    }

}

export class IconEdit extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="mode_edit" />
        );
    }

}

/**
 * Обновление
 * https://design.google.com/icons/#ic_refresh
 * 
 * @export
 * @class IconRefresh
 * @extends {React.Component<{}, {}>}
 */
export class IconRefresh extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="refresh" />
        );
    }

}

/**
 * Еще
 * https://design.google.com/icons/#ic_expand_more
 * 
 * @export
 * @class IconMore
 * @extends {React.Component<{}, {}>}
 */
export class IconMore extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="expand_more" />
        );
    }

}

export class IconPhoto extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="photo_camera" />
        );
    }

}

export class IconMenu extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="menu" />
        );
    }

}

export class IconAdd extends React.Component<{}, {}> {

    render() {
        return (
            <Icon name="add" />
        );
    }

}