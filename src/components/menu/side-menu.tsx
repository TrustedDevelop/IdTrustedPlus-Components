import * as React from "react";
import { Link } from "react-router";
import { Icon } from "../icon";

export interface IMenuItemProps {
    /**
     * Текст элемента
     * 
     * @type {string}
     * @memberOf IMenuItemProps
     */
    text: string;
    onClick?: React.MouseEventHandler<any>;
    to?: string;
    href?: string;
    /**
     * Название иконки с material icon
     * 
     * @type {string}
     * @memberOf IMenuItemProps
     */
    icon?: string;
}

export class MenuItem extends React.Component<IMenuItemProps, {}> {

    constructor(props: IMenuItemProps) {
        super(props);
        this.state = {};
        this.onClickHandle = this.onClickHandle.bind(this);
    }

    onClickHandle(e: React.MouseEvent<any>) {
        e.stopPropagation();
        this.props.onClick && this.props.onClick(e);
        document.body.click();
    }

    render() {
        return (
            this.props.to ?
                <Link activeClassName={"active"}
                    className="menu-item"
                    to={this.props.to}>
                    {this.props.icon ? <Icon name={this.props.icon} /> : null}
                    <span>{this.props.text}</span>
                </Link>
                :
                <a className="menu-item" href={this.props.href} onClick={this.onClickHandle}>
                    {this.props.icon ? <Icon name={this.props.icon} /> : null}
                    <span>{this.props.text}</span>
                </a>
        );
    }
}


export class AppLeftMenuHeader extends React.Component<any, any> {

    render() {
        return (
            <div className="side-menu-header">
                {this.props.children}
            </div>
        );
    }
}
export class AppLeftMenuContent extends React.Component<any, any> {

    render() {
        return (
            <div className="menu-list">
                {this.props.children}
            </div>
        );
    }
}

export interface AppLeftMenuFooterProps {
    title: string;
}
export class AppLeftMenuFooter extends React.Component<AppLeftMenuFooterProps, any> {
   constructor(props: AppLeftMenuFooterProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="side-menu-footer">
                <div >{this.props.title}</div>
                <div className="links">
                    {this.props.children}
                </div>
            </div>
        );
    }
}


/**
 * Компонент гамбургер меню
 */
export interface IAppLeftMenuProps { }

export interface IAppLeftMenuState {
    show?: boolean;
    hidden?: boolean;
}

export class AppLeftMenu extends React.Component<IAppLeftMenuProps, IAppLeftMenuState> {

    constructor(props: IAppLeftMenuProps) {
        super(props);
        this.state = {
            show: false,
            hidden: true
        };
        this.toggle = this.toggle.bind(this);
        this.onWindowClick = this.onWindowClick.bind(this);
    }

    protected onWindowClick() {
        if (!this.state.hidden)
            this.toggle();
    }

    componentDidMount() {
        window.addEventListener("click", this.onWindowClick);
    }
    componentWillUnmount() {
        window.removeEventListener("click", this.onWindowClick);
    }

    toggle() {
        if (this.state.hidden) {
            this.setState({
                hidden: false
            }, () => {
                // Скрыть элемент после анимации
                setTimeout(() => {
                    this.setState({
                        show: true
                    });
                }, 100);
            });
        } else {
            this.setState({
                show: false
            }, () => {
                setTimeout(() => {
                    this.setState({
                        hidden: true
                    });
                }, 500);
            });
        }
    }

    render() {
        return (
            <div className={`bg ${this.state.hidden ? "hidden" : ""} ${this.state.show ? "show" : ""}`}>
                <div className={`side-menu ${this.state.show ? "show" : ""}`}>
                    {this.props.children}
                </div>
            </div >
        );
    }

}
