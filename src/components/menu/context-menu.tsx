import * as React from "react";

export interface IContextMenuProps { }

export interface IContextMenuState {
    hidden?: boolean;
    right?: number;
    top?: number;
}

export class ContextMenu extends React.Component<IContextMenuProps, IContextMenuState> {

    constructor(props: IContextMenuProps) {
        super(props);
        this.state = {
            hidden: true,
            right: 0,
            top: 0,
        };

        this.click = this.click.bind(this);
        this.onWindowClickHandle = this.onWindowClickHandle.bind(this);
    }

    click(e: React.MouseEvent<any>) {
        document.body.click();
        e.stopPropagation();
        this.setState({
            hidden: !this.state.hidden
        });
    }

    onWindowClickHandle() {
        if (!this.state.hidden)
            this.setState({ hidden: !this.state.hidden });
    }

    componentDidMount() {
        window.addEventListener("click", this.onWindowClickHandle);
    }

    componentWillUnmount() {
        window.removeEventListener("click", this.onWindowClickHandle);
    }

    render() {
        return (
            <div className="context-menu" hidden={this.state.hidden}>
                {this.props.children}
            </div>
        );
    }

}

