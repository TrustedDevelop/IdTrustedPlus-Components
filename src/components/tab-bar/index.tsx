import * as React from "react";
import { Link } from "react-router";

export interface ITabItem {
    text: string;
    link: string;
}

export interface ITabBarProps {
    items: ITabItem[];
}

export class TabBar extends React.Component<ITabBarProps, {}> {

    constructor(props: ITabBarProps) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className="tab-bar">
                {this.props.items.map((item, index) =>
                    <Link className={`tab-bar-item`} key={index} activeClassName="active" to={item.link}>{item.text}</Link>
                )}
            </div>
        );
    }
}