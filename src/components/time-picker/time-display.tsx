import * as React from "react";
import {moment, Affix, ClockMode} from "./index";
import * as classnames from "classnames";

interface ITimeDisplayProps {
    initialTime: string;
    onSelectAffix: (affix: Affix) => void;
    onSelectHour: () => void;
    onSelectMin: () => void;
    mode: ClockMode;
    affix: Affix;
}

export class TimeDisplay extends React.Component<ITimeDisplayProps, {}> {
    render() {
        const {affix, initialTime, onSelectAffix, onSelectHour, onSelectMin, mode} = this.props;
        const time = moment(initialTime);
        const hours = time.hours();
        const hoursLabel = (hours === 12 || hours === 0) ? affix === "am" ? 0 : 12 : hours % 12;
        const minutes = time.format("mm");
        return (
            <div className="display-time--root">
                <div className="display-time--content">
                    <div className={classnames("display-hours", {"clock-display-time--item-active": mode === "h"})}
                         onClick={onSelectHour}>
                        {hoursLabel}
                    </div>
                    <div className="time-separator">:</div>
                    <div className={classnames("display-minutes", {"clock-display-time--item-active": mode === "m"})}
                         onClick={onSelectMin}>
                        {minutes}
                    </div>
                </div>
                <div className="select-affix">
                    <div onClick={() => onSelectAffix("am")}
                         className={classnames("affix-value affix-am", {"affix-current": affix === "am"})}>am
                    </div>
                    <div onClick={() => onSelectAffix("pm")}
                         className={classnames("affix-value affix-pm", {"affix-current": affix === "pm"})}>pm
                    </div>
                </div>
            </div>
        );
    }
}