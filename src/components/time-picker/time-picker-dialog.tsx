import * as React from "react";
import {findDOMNode} from "react-dom";
import * as classnames from "classnames";

import {moment} from "./index";
import {FlatButton} from "../button/index";
import {Clock} from "./clock";

interface ITimePickerDialogProps {
    initialTime: string;
    autoSelect: boolean;
    cancelLabel: string;
    okLabel: string;
    onAccept: (time: string) => void;
    onDismiss: () => void;
    onShow: () => void;
    mode: "portrait" | "landscape";
}

interface ITimePickerDialogState {
    isOpened: boolean;
}

export class TimePickerDialog extends React.Component<ITimePickerDialogProps, ITimePickerDialogState> {
    constructor(props: ITimePickerDialogProps) {
        super(props);

        this.state = {
            isOpened: false
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleAccept = this.handleAccept.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
        this.handleChangeMinutes = this.handleChangeMinutes.bind(this);
        this.handleWrapperClick = this.handleWrapperClick.bind(this);
        this.handleWindowKeyUp = this.handleWindowKeyUp.bind(this);
    }

    open() {
        this.setState({
            isOpened: true
        }, () => {
            (findDOMNode(this.refs["root"]) as any).focus();
        });
    }

    close() {
        this.props.onDismiss && this.props.onDismiss();
        this.setState({
            isOpened: false
        });
    }

    private handleAccept() {
        const time = (this.refs["clock"] as Clock).getTime();
        this.props.onAccept && this.props.onAccept(time);
        this.setState({
            isOpened: false
        })
    }

    private handleWindowKeyUp(event: any) {
        const {keyCode} = event;
        switch (keyCode) {
            case 13:
                this.handleAccept();
                break;
            case 27:
                this.handleDismiss();
                break;
            default:
                break;
        }
        event.preventDefault();
        event.stopPropagation();
        return false;
    }


    private handleDismiss() {
        (this.refs["clock"] as Clock).setMode("h");
        this.props.onDismiss && this.props.onDismiss();
        this.setState({
            isOpened: false
        });
    }

    private handleWrapperClick(event: any) {
        const {target} = event;
        if (target.classList.contains("time-picker-dialog-wrapper")) {
            this.handleDismiss()
        }
    }

    private handleChangeMinutes(time: string) {
        if (this.props.autoSelect) {
            this.handleAccept();
        }
    }

    render() {
        const {cancelLabel, okLabel, autoSelect, initialTime, mode} = this.props;
        return (
            <div
                className={classnames("time-picker-dialog-root", {opened: this.state.isOpened})}>
                <div className="time-picker-dialog-wrapper"
                     ref="root"
                     tabIndex={2}
                     onClick={this.handleWrapperClick}>
                    <div className="time-picker-dialog-content">
                        <Clock
                            viewMode={mode}
                            ref="clock"
                            autoSelect={autoSelect}
                            initialTime={initialTime}
                            onAcceptClick={this.handleAccept}
                            onDismissClick={this.handleDismiss}
                            cancelLabel={cancelLabel}
                            okLabel={okLabel}
                            onChangeMinutes={this.handleChangeMinutes}/>
                    </div>
                </div>
            </div>
        )
    }
}
