import * as React from "react";

import { ComponentBase } from "../types";
import { Title } from "../text";
import { IconMenu } from "../icon/index";

export interface IAppBarHeaderProps {
    title: string;
    centerBlock?: JSX.Element;
    rightBlock?: JSX.Element[];
    onNavMenuClick?: React.MouseEventHandler<any>;
}


export class AppBarHeader extends React.Component<IAppBarHeaderProps, {}> {

    constructor(props: IAppBarHeaderProps) {
        super(props);
        this.state = {};
    }

    render() {

        return (
            <div className="app-bar-header-main">
                <div className="left-block">
                    <div className="btn-menu" onClick={this.props.onNavMenuClick}><IconMenu /></div>
                    <div className="app-header-title">{this.props.title}</div>
                    <div className="title-spliter"></div>
                </div>
                <div className="center-block">
                    {this.props.centerBlock}
                </div>
                <div className="right-block">
                    {this.props.rightBlock && this.props.rightBlock.map((item, index) =>
                        React.cloneElement(item, { key: `right_block_item_${index}` })
                    )}
                </div>
            </div>
        );
    }
}

export interface IAppBarContentProps { }

export class AppBarContent extends React.Component<IAppBarContentProps, {}> {

    constructor(props: IAppBarContentProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="app-bar-content">
                {this.props.children}
            </div>
        );
    }

}

export interface IAppBarProps extends ComponentBase {
    title?: string;
    onNavMenuClick?: React.MouseEventHandler<any>;
}

export class AppBar extends React.Component<IAppBarProps, {}> {
    static defaultProps: IAppBarProps = {
        className: ""
    };

    render() {
        const {className} = this.props;
        return (
            <div className={`${className}`}>
                {this.props.children}
            </div>
        );
    }
}