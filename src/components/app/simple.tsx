import * as React from "react";
import * as classnames from "classnames";

import { ComponentBase, ComponentTitle } from "../types";
import { AppBar, AppBarContent, AppBarHeader } from "./app-bar";
import { Title } from "../text";

export interface IAppBArSimpleProps extends ComponentBase, ComponentTitle {
    onNavMenuClick: React.MouseEventHandler<any>;
    centerBlock?: JSX.Element;
    rightBlock?: JSX.Element[];
}

export class AppBarSimple extends React.Component<IAppBArSimpleProps, {}> {

    render() {

        const name = classnames(["app-bar"]);

        return (
            <AppBar className={name}>
                <AppBarHeader
                    title={this.props.label}
                    onNavMenuClick={this.props.onNavMenuClick}
                    centerBlock={this.props.centerBlock}
                    rightBlock={this.props.rightBlock}
                    />

                <AppBarContent>
                    {this.props.children}
                </AppBarContent>
            </AppBar>
        );
    }

}