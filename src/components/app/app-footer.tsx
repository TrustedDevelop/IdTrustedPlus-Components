import * as React from "react";

export interface IFooterProps {
    title: string;
}

export class Footer extends React.Component<IFooterProps, {}> {

    constructor(props: IFooterProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="footer">
                <div className="container">
                    <div>{this.props.title}</div>
                    <div className="links">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }

}