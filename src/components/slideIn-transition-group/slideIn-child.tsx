import * as React from "react";

export interface ISlideInChildProps {
    key?: any;
    enterDelay?: any;
    maxScale?: any;
    minScale?: any;
    className?: string;
}

export class SlideInChild extends React.Component<ISlideInChildProps, {}> {
    render() {
        const {children} = this.props;
        return (
            <div key={Math.random().toString(16).substr(-12)} className={this.props.className}>
                {children}
            </div>
        );
    }
}