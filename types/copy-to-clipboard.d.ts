declare module "copy-to-clipboard" {
    interface Options {
        debug?: boolean;
        message?: string;
    }

    interface Copy {
        (text: string, options?: Options): boolean;
    }

    const copy: Copy;
    export = copy;
}