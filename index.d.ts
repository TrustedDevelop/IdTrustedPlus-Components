import * as React from "react";

export = TrustedComponents;
export as namespace TrustedComponents;

declare type Assoc<T> = { [key: string]: T }
declare type TooltipPosition = "right" | "bottom" | "left";

declare namespace TrustedComponents {

    // core

    export class Browser {
        static isOpera(): boolean;

        static isFirefox(): boolean;

        /**
         * At least Safari 3+: "[object HTMLElementConstructor]"
         *
         * @static
         * @returns {boolean}
         */
        static isSafari(): boolean;

        /**
         * Internet Explorer 6-11
         *
         * @static
         * @returns {boolean}

         */
        static isIE(): boolean;

        /**
         * Edge 20+
         *
         * @static
         * @returns
         */
        static isEdge(): boolean;

        /**
         * Chrome 1+
         *
         * @static
         * @returns {boolean}
         */
        static isChrome(): boolean;

        static isBlink(): boolean;
    }

    export function RandomId(size?: number): string;

    export class AssocArray {
        static size(assoc: Assoc<any>): number;

        static forEach<T>(assoc: Assoc<T>, cb: (item: T, index: string) => void): void;
        static forEach(assoc: Assoc<any>, cb: (item: any, index: string) => void): void;

        static map<T, U>(assoc: Assoc<T>, cb: (item: T, index: string) => U): U[];
        static map<U>(assoc: Assoc<any>, cb: (item: any, index: string) => U): U[];

        static filter<T>(assoc: Assoc<T>, cb: (item: any, index: string) => boolean): Assoc<T>;
        static filter(assoc: Assoc<any>, cb: (item: any, index: string) => boolean): Assoc<any>;

        static sort<T>(assoc: Assoc<T>, cb: (a: {
                                                 key: string;
                                                 value: T;
                                             }, b: {
                                                 key: string;
                                                 value: T;
                                             }) => number): Assoc<T>;
    }
    /**
     * Возвращает первого родителя элемента с заданным именем класса
     * - если родитель не найден, то возвращает null
     *
     * @export
     * @param {HTMLElement} element
     * @param {string} name
     * @returns {HTMLElement}
     */
    export function getParentByClass(element: HTMLElement, name: string): HTMLElement | null;

    /**
     * Экранирование символов в регулярном выражении
     *
     * @param {string} text
     * @returns
     */
    export function RegExpEscape(text: string): string;

    /**
     *
     *
     * @export
     * @param {string} link
     * @param {number} [width=495]
     * @param {number} [height=495]
     */
    export function windowOpener(link: string, width?: number, height?: number): void;

    /**
     * Объединяет несколько объектов в один
     *
     * @export
     * @param {*} target Объект в который будут добавлены новые свойства.
     * @param {...any[]} sources Объекты, свойства которых будут добавлены в объект `target`
     * @returns Возвращает расширенный объект `target`
     */
    export function assign(target: any, ...sources: any[]): any;

    // types

    export type ComponentSizeType = "small" | "medium" | "large";
    export type ComponentFontSizeType =
        "display-1"
        | "display-2"
        | "display-3"
        | "display-4"
        | "headline"
        | "title"
        | "subheading"
        | "body-1"
        | "body-2"
        | "caption"
        | "button";
    export interface ComponentBase {
        className?: string;
    }
    export interface ComponentSize {
        size?: ComponentSizeType;
    }
    export interface ComponentFontSize {
        fontSize?: ComponentFontSizeType;
    }
    export interface ComponentColor {
        color?: string;
    }
    export interface ComponentColorSheme {
        colorSheme?: string;
    }
    export interface ComponentBgColor {
        bgColor?: string;
    }
    export interface ComponentTitle {
        label: string;
        description?: string;
    }

    // app

    export interface IAppBarHeaderProps {
        title: string;
        centerBlock?: JSX.Element;
        rightBlock?: JSX.Element[];
        onNavMenuClick?: React.MouseEventHandler<any>;
    }
    export class AppBarHeader extends React.Component<IAppBarHeaderProps, {}> {
        constructor(props: IAppBarHeaderProps);

        render(): JSX.Element;
    }
    export interface IAppBarContentProps {
    }
    export class AppBarContent extends React.Component<IAppBarContentProps, {}> {
        constructor(props: IAppBarContentProps);

        render(): JSX.Element;
    }
    export interface IAppBarProps extends ComponentBase {
        title?: string;
        onNavMenuClick?: React.MouseEventHandler<any>;
    }
    export class AppBar extends React.Component<IAppBarProps, {}> {
        static defaultProps: IAppBarProps;

        render(): JSX.Element;
    }

    export interface IFooterProps {
        title: string;
    }
    export class Footer extends React.Component<IFooterProps, {}> {
        constructor(props: IFooterProps);

        render(): JSX.Element;
    }

    export interface IAppBArSimpleProps extends ComponentBase, ComponentTitle {
        onNavMenuClick: React.MouseEventHandler<any>;
        centerBlock?: JSX.Element;
        rightBlock?: JSX.Element[];
    }
    export class AppBarSimple extends React.Component<IAppBArSimpleProps, {}> {
        render(): JSX.Element;
    }

    // button

    export interface IButtonGroupProps extends ComponentBase {
        position?: "top" | "bottom";
        align?: "left" | "center" | "right";
    }
    export class ButtonGroup extends React.Component<IButtonGroupProps, {}> {
        static defaultProps: IButtonGroupProps;

        render(): JSX.Element;
    }
    export interface IBaseButtonProps {
        color?: string;
        title?: string;
        bgColor?: string;
        onClick?: React.MouseEventHandler<any>;
        disabled?: boolean;
        className?: string;
        to?: string;
        hidden?: boolean;
        href?: string;
        target?: "_blank" | "_self" | "_parent" | "_top";
        position?: "left" | "center" | "right";
        tooltip?: string;
        tooltipPosition?: TooltipPosition
    }
    export interface IFlatButtonProps extends IBaseButtonProps {
    }
    export class FlatButton extends React.Component<IFlatButtonProps, {}> {
        static defaultProps: IRaisedButtonProps;

        render(): JSX.Element;
    }
    export interface IRaisedButtonProps extends IBaseButtonProps {
    }
    export class RaisedButton extends React.Component<IRaisedButtonProps, {}> {
        static defaultProps: IRaisedButtonProps;

        render(): JSX.Element;
    }
    export interface IRoundButtonProps extends IBaseButtonProps {
        size?: ComponentSizeType;
    }
    export class RoundButton extends React.Component<IRoundButtonProps, {}> {
        static defaultProps: IRoundButtonProps;

        render(): JSX.Element;
    }
    export interface IIconButtonBaseProps extends IRoundButtonProps {
    }
    export interface IIconButtonProps extends IIconButtonBaseProps {
        icon: string;
    }
    export class IconButton extends React.Component<IIconButtonProps, {}> {
        static defaultProps: IIconButtonProps;

        render(): JSX.Element;
    }
    export interface ICopyButtonProps extends IIconButtonBaseProps {
        text: string;
    }
    export class CopyButton extends React.Component<ICopyButtonProps, {}> {
        render(): JSX.Element;
    }
    export interface IEditButtonProps extends IIconButtonBaseProps {
    }
    export class EditButton extends React.Component<IEditButtonProps, {}> {
        render(): JSX.Element;
    }
    export class RefreshButton extends React.Component<IRoundButtonProps, {}> {
        render(): JSX.Element;
    }
    export class MoreButton extends React.Component<IRoundButtonProps, {}> {
        render(): JSX.Element;
    }
    export interface IPhotoButtonProps extends IIconButtonBaseProps {
    }
    export class PhotoButton extends React.Component<IPhotoButtonProps, {}> {
        render(): JSX.Element;
    }
    export interface IAddButtonProps extends IIconButtonBaseProps {
    }
    export class AddButton extends React.Component<IAddButtonProps, {}> {
        static defaultProps: {
            color: string;
            title: string;
        };

        render(): JSX.Element;
    }
    export interface IActionButtonProps extends IRoundButtonProps {
    }
    export class ActionButton extends React.Component<IActionButtonProps, {}> {
        static defaultProps: {
            color: string;
            size: string;
        };

        render(): JSX.Element;
    }
    export interface IFixedCreateProps extends IRoundButtonProps {
    }
    export class FixedCreate extends React.Component<IFixedCreateProps, {}> {
        render(): JSX.Element;
    }

    // chip

    export interface IChipProps {
        /**
         * Отображаемое имя
         *
         * @type {string}
         * @memberOf IUserChipProps
         */
        text: string;
        /**
         * Картинка
         *
         * @type {string}
         * @memberOf IUserChipProps
         */
        image?: string;
        /**
         * Картинка по умолчанию
         *
         * @type {string}
         * @memberOf IChipProps
         */
        defaultImage?: string;
        /**
         * Обработчик нажатия кнопки Закрыть
         * - Если значение отсутствует, то кнопка Закрыть будет недоступна
         *
         * @type {React.MouseEventHandler<any>}
         * @memberOf IUserChipProps
         */
        onCloseClick?: React.MouseEventHandler<any>;
    }
    export class Chip extends React.Component<IChipProps, {}> {
        constructor(props: IChipProps);

        onCloseClick(e: React.MouseEvent<any>): void;

        render(): JSX.Element;
    }


    // Portal

    export abstract class Portal<P, S> extends React.Component<P, S> {
        protected name: string;
        protected node: HTMLElement;

        constructor(portalName: string);

        abstract componentRender(): JSX.Element;

        componentDidUpdate(): void;

        render(): null;
    }

    // empty list

    export interface IEmptyListProps {
        title: string;
        description: string;
        icon: string;
    }
    export class EmptyList extends React.Component<IEmptyListProps, {}> {
        constructor(props: IEmptyListProps);

        render(): JSX.Element;
    }

    // icon

    export interface IIconProps {
        name: string;
        color?: string;
        desc?: string;
    }
    export class Icon extends React.Component<IIconProps, {}> {
        constructor(props: IIconProps);

        static defaultProps: IIconProps;

        render(): JSX.Element;
    }
    export class IconEdit extends React.Component<{}, {}> {
        render(): JSX.Element;
    }
    /**
     * Обновление
     * https://design.google.com/icons/#ic_refresh
     *
     * @export
     * @class IconRefresh
     * @extends {React.Component<{}, {}>}
     */
    export class IconRefresh extends React.Component<{}, {}> {
        render(): JSX.Element;
    }
    /**
     * Еще
     * https://design.google.com/icons/#ic_expand_more
     *
     * @export
     * @class IconMore
     * @extends {React.Component<{}, {}>}
     */
    export class IconMore extends React.Component<{}, {}> {
        render(): JSX.Element;
    }
    export class IconPhoto extends React.Component<{}, {}> {
        render(): JSX.Element;
    }
    export class IconMenu extends React.Component<{}, {}> {
        render(): JSX.Element;
    }
    export class IconAdd extends React.Component<{}, {}> {
        render(): JSX.Element;
    }

    // input

    export interface IInputFieldProps {
        className?: string;
        hidden?: boolean;
        readOnly?: boolean;
        autoFocus?: boolean;
        placeholder?: string;
        error?: string;
        name?: string;
        onKeyUp?: React.KeyboardEventHandler<HTMLInputElement>;
        onChange?: React.FormEventHandler<HTMLInputElement>;
        onBlur?: React.FocusEventHandler<HTMLInputElement>;
        onInput?: React.FocusEventHandler<HTMLInputElement>;
        onClick?: React.MouseEventHandler<HTMLInputElement>;
        disabled?: boolean;
        value?: string;
        defaultValue?: string;
        label?: string;
        required?: boolean;
        description?: string;
        type?: string;
        tabIndex?: number;
        autoComplete?: boolean;
        fontSize?:
            "display-1"
            | "display-2"
            | "display-3"
            | "display-4"
            | "headline"
            | "title"
            | "subheading"
            | "body-1"
            | "body-2"
            | "caption"
            | "button"
            | "input";
        color?: string;
        colorSheme?: ComponentColorSheme;
        icon?: string;
    }
    export interface IInputFieldState {
        focused?: boolean;
    }
    export class InputField extends React.Component<IInputFieldProps, IInputFieldState> {
        static defaultProps: IInputFieldProps;
        id: string;

        constructor(props: IInputFieldProps);

        input: HTMLInputElement;

        onFocusHandle(e: React.FocusEvent<HTMLInputElement>): void;

        onBlurHandle(e: React.FocusEvent<HTMLInputElement>): void;

        render(): JSX.Element;
    }
    export interface IInputAvatarProps {
        onChange?: React.FormEventHandler<HTMLInputElement>;
        image?: File | string;
        size?: ComponentSizeType;
        defaultImage?: string;
    }
    export class InputAvatar extends React.Component<IInputAvatarProps, {}> {
        static defaultProps: IInputAvatarProps;

        constructor(props: IInputAvatarProps);

        onClick(e: React.MouseEvent<any>): void;

        input: HTMLInputElement;

        render(): JSX.Element;
    }
    export interface ICheckBoxProps {
        checked?: boolean;
        defaultChecked?: boolean;
        label?: string;
        onChange?: React.FormEventHandler<HTMLInputElement>;
        disabled?: boolean;
        className?: string;
    }
    export class CheckBox extends React.Component<ICheckBoxProps, {}> {
        id: string;

        render(): JSX.Element;
    }
    export interface IInputSelectProps {
        value?: string;
        defaultValue?: string;
        placeholder?: string;
        onChange?: React.FormEventHandler<HTMLInputElement>;
        error?: string;
    }
    export interface IInputSelectState {
        context?: boolean;
    }
    export class InputSelect extends React.Component<IInputSelectProps, IInputSelectState> {
        context: HTMLElement;
        main: HTMLDivElement;

        constructor(props: IInputSelectProps);

        /**
         * Выводит Context на экран
         * - Если элемент на экране, то ничего не происходит
         *
         * @protected
         *
         * @memberOf InputSelect
         */
        protected showContext(visible: boolean): void;

        protected onWindowClickHandler(): void;

        onClick(e: React.MouseEvent<any>): void;

        componentDidMount(): void;

        componentWillUnount(): void;

        protected onBlurHandler(e: React.FocusEvent<any>): void;

        protected onInputHandler(e: React.FocusEvent<any>): void;

        render(): JSX.Element;
    }
    export interface IInputCaptchaProps {
        name?: string;
        sitekey: string;
    }
    export interface IInputCaptchaState {
    }
    export interface RecaptchaMessage<T> {
        message?: T;
        messageType: string;
    }
    export interface RecaptchaMessageReady extends RecaptchaMessage<any> {
        messageType: "ready_anchor";
    }
    export interface RecaptchaClientData {
        Ld?: any;
        Ul: number[];
    }
    export interface RecaptchaMessageClientData extends RecaptchaMessage<RecaptchaClientData> {
        messageType: "client_data";
    }
    export interface RecaptchaLoadChallenge {
        tk: {
            left: number;
            top: number;
            width: number;
            height: number;
        };
        Pi: {
            src: string;
            title: string;
        };
        Gh: string;
    }
    export interface RecaptchaMessageLoadChallenge extends RecaptchaMessage<RecaptchaLoadChallenge> {
        messageType: "load_challenge";
    }
    export interface RecaptchaShowChallenge {
        visible: boolean;
        Ld: any;
        af: {
            width: number;
            height: number;
        };
    }
    export interface RecaptchaMessageShowChallenge extends RecaptchaMessage<RecaptchaShowChallenge> {
        messageType: "show_challenge";
    }
    export interface RecaptchaToken {
        response: string;
        Sl: number;
    }
    export interface RecaptchaMessageToken extends RecaptchaMessage<RecaptchaToken> {
        messageType: "token";
    }
    export interface RecaptchaMessageExpiry extends RecaptchaMessage<any> {
        messageType: "expiry";
    }
    export class InputCaptcha extends React.Component<IInputCaptchaProps, IInputCaptchaState> {
        id: string;

        constructor(props: IInputCaptchaProps);

        onMessageHandler(e: MessageEvent): void;

        componentDidMount(): void;

        componentWillUnount(): void;

        readonly value: string;
        static defaultProps: IInputCaptchaProps;

        render(): JSX.Element;
    }
    export interface InputRadioProps extends React.InputHTMLAttributes<HTMLInputElement> {
        label?: string;
        className?: string;
    }
    export class InputRadio extends React.Component<InputRadioProps, {}> {
        protected id: string;

        render(): JSX.Element;
    }

    // list

    export interface IListProps extends ComponentBase {
    }
    export class List extends React.Component<IListProps, {}> {
        render(): JSX.Element;
    }
    export interface IListHeaderProps extends ComponentBase {
    }
    export class ListHeader extends React.Component<IListHeaderProps, {}> {
        elements: JSX.Element[];

        render(): JSX.Element;
    }
    export type ListItemType = "icon" | "name" | "cell" | "menu";
    export type ListItemSort = "less" | "more";
    export interface IListHeaderItemProps {
        type: ListItemType;
        text: string;
        sort?: ListItemSort;
        onClick?: React.MouseEventHandler<any>;
        className?: string;
    }
    export class ListHeaderItem extends React.Component<IListHeaderItemProps, {}> {
        render(): JSX.Element;
    }
    export interface IListHeaderCustomItemProps {
        type: ListItemType;
        className?: string;
    }
    export class ListHeaderCustomItem extends React.Component<IListHeaderCustomItemProps, {}> {
        constructor(props: IListHeaderCustomItemProps);

        render(): JSX.Element;
    }
    export interface IListItemProps {
        onClick?: React.MouseEventHandler<any>;
        to?: string;
        tooltip?: string;
        tooltipPosition?: TooltipPosition;
    }
    export class ListItem extends React.Component<IListItemProps, {}> {
        constructor(props: IListItemProps);

        onClick(e: React.MouseEvent<any>): void;

        render(): JSX.Element;
    }
    export interface IListCustomProps {
        type: ListItemType;
        className?: string;
    }
    export class ListCustom extends React.Component<IListCustomProps, {}> {
        render(): JSX.Element;
    }
    export interface IListIconProps {
        icon: string;
        color?: string;
    }
    export class ListIcon extends React.Component<IListIconProps, {}> {
        render(): JSX.Element;
    }
    export interface IListIconTextProps {
        text: string;
        color?: string;
    }
    export class ListIconText extends React.Component<IListIconTextProps, {}> {
        render(): JSX.Element;
    }
    export interface IListImageProps {
        image: string;
        defaultImage?: string;
    }
    export class ListImage extends React.Component<IListImageProps, {}> {
        render(): JSX.Element;
    }
    export interface IListCellProps {
        className?: string;
        text?: string;
    }
    export class ListCell extends React.Component<IListCellProps, {}> {
        render(): JSX.Element;
    }
    export interface IListNameProps {
        label: string;
        description: string;
        subDescription?: string;
    }
    export class ListName extends React.Component<IListNameProps, {}> {
        constructor(props: IListNameProps);

        render(): JSX.Element;
    }
    export interface IListMenuProps {
    }
    export class ListMenu extends React.Component<IListMenuProps, {}> {
        constructor(props: IListMenuProps);

        protected menu: any;

        onClick(e: React.MouseEvent<any>): void;

        render(): JSX.Element;
    }
    export interface IListActionProps {
        icon: string;
        color?: string;
        onClick: React.MouseEventHandler<any>;
    }
    export class ListAction extends React.Component<IListActionProps, {}> {
        constructor(props: IListActionProps);

        protected menu: any;

        onClick(e: React.MouseEvent<any>): void;

        render(): JSX.Element;
    }
    export interface IListMoreProps {
        /**
         * Обработчик события нажатия по кнопке
         *
         * @type {React.MouseEventHandler<any>}
         * @memberOf IListMoreProps
         */
        onClick?: React.MouseEventHandler<any>;
        /**
         * Задает свойство видимости объекта
         *
         * @type {boolean}
         * @memberOf IListMoreProps
         */
        hidden?: boolean;
    }
    /**
     * Отображает юблок с кнопкой подгрузки списка
     *
     * @export
     * @class ListMore
     * @extends {React.Component<IListMoreProps, IListMoreState>}
     */
    export class ListMore extends React.Component<IListMoreProps, {}> {
        render(): JSX.Element;
    }

    // menu

    export interface IContextMenuProps {
    }
    export interface IContextMenuState {
        hidden?: boolean;
        right?: number;
        top?: number;
    }
    export class ContextMenu extends React.Component<IContextMenuProps, IContextMenuState> {
        constructor(props: IContextMenuProps);

        click(e: React.MouseEvent<any>): void;

        onWindowClickHandle(): void;

        componentDidMount(): void;

        componentWillUnmount(): void;

        render(): JSX.Element;
    }
    export interface IMenuItemProps {
        /**
         * Текст элемента
         *
         * @type {string}
         * @memberOf IMenuItemProps
         */
        text: string;
        onClick?: React.MouseEventHandler<any>;
        to?: string;
        href?: string;
        /**
         * Название иконки с material icon
         *
         * @type {string}
         * @memberOf IMenuItemProps
         */
        icon?: string;
    }
    export class MenuItem extends React.Component<IMenuItemProps, {}> {
        constructor(props: IMenuItemProps);

        onClickHandle(e: React.MouseEvent<any>): void;

        render(): JSX.Element;
    }
    export class AppLeftMenuHeader extends React.Component<any, any> {
        render(): JSX.Element;
    }
    export class AppLeftMenuContent extends React.Component<any, any> {
        render(): JSX.Element;
    }
    export class AppLeftMenuFooter extends React.Component<any, any> {
        render(): JSX.Element;
    }
    /**
     * Компонент гамбургер меню
     */
    export interface IAppLeftMenuProps {
    }
    export interface IAppLeftMenuState {
        show?: boolean;
        hidden?: boolean;
    }
    export class AppLeftMenu extends React.Component<IAppLeftMenuProps, IAppLeftMenuState> {
        constructor(props: IAppLeftMenuProps);

        protected onWindowClick(): void;

        componentDidMount(): void;

        componentWillUnmount(): void;

        toggle(): void;

        render(): JSX.Element;
    }

    // modal

    export interface IModalProps extends ComponentBase {
        onHide?: () => void;
    }
    export interface IModalState {
        show?: boolean;
        hidden?: boolean;
    }
    export class Modal extends Portal<IModalProps, IModalState> {
        protected node: HTMLElement;

        constructor(props: IModalProps);

        componentDidMount(): void;

        componentWillUnmount(): void;

        /**
         * Отобразить модальное окно
         */
        show(): void;

        /**
         * Скрыть модальное окно
         */
        hide(): void;

        onBackgroundClick(e: React.MouseEvent<HTMLDivElement>): void;

        onModalClick(e: React.MouseEvent<HTMLDivElement>): void;

        onKeyUp(e: React.KeyboardEvent<HTMLInputElement>): void;

        componentRender(): JSX.Element;
    }
    export type ModalMessageType = "info" | "error" | "warn" | "question";
    export interface IModalMessageProps extends ComponentBase {
    }
    export interface IModalMessageState extends ComponentBase {
        label?: string;
        message: string;
        type?: ModalMessageType;
        /**
         * Обработчик закрытия
         *
         * @memberOf IModalMessageProps
         */
        onResult?: (result: boolean) => void;
    }
    export class ModalMessage extends React.Component<IModalMessageProps, IModalMessageState> {
        static defaultState: IModalMessageState;
        protected modal: Modal;

        constructor(props: IModalMessageProps);

        show(params: IModalMessageState): void;

        hide(): void;

        render(): JSX.Element;
    }

    // page

    export interface IPageGroupProps {
        /**
         * Тип группировки
         * - row: группировка по ряду. По умолчанию
         * - col: группировка по колонке
         *
         * @type {("row" | "col")}
         */
        type?: "row" | "col";
        className?: string;
    }
    export class PageGroup extends React.Component<IPageGroupProps, {}> {
        render(): JSX.Element;
    }
    export interface IHeaderProps extends ComponentBase, ComponentSize, ComponentColor, ComponentBgColor {
        bgImage?: string;
    }
    export class PageHeader extends React.Component<IHeaderProps, {}> {
        static defaultProps: IHeaderProps;

        render(): JSX.Element;
    }
    export interface IContentProps extends ComponentBase, ComponentColor, ComponentBgColor {
    }
    export class PageContent extends React.Component<IContentProps, {}> {
        static defaultProps: IContentProps;

        render(): JSX.Element;
    }
    export interface IContentProps extends ComponentBase, ComponentColor, ComponentBgColor {
    }
    export class PageFooter extends React.Component<IContentProps, {}> {
        static defaultProps: IContentProps;

        render(): JSX.Element;
    }
    export interface IPageProps extends ComponentBase {
        title?: string;
    }
    export class Page extends React.Component<IPageProps, {}> {
        static defaultProps: IPageProps;

        render(): JSX.Element;
    }
    export interface IPageSimpleProps extends ComponentBase, ComponentTitle {
    }
    export class PageSimple extends React.Component<IPageSimpleProps, {}> {
        render(): JSX.Element;
    }

    export interface IPreloaderProps extends ComponentBase, ComponentSize {
        hidden?: boolean;
    }

    // preloader

    export interface IPreloaderState {
        hide: boolean;
        show: boolean;
    }
    export class Preloader extends React.Component<IPreloaderProps, IPreloaderState> {
        static defaultProps: IPreloaderProps;

        constructor(props: IPreloaderProps);

        render(): JSX.Element;
    }

    // property

    export interface IPropertyViewProps {
    }
    export class PropertyView extends React.Component<IPropertyViewProps, {}> {
        render(): JSX.Element;
    }
    export interface IPropertyViewItemProps {
        title: string;
        description?: string;
        value: string | number;
    }
    /**
     * Дочерний элемент PropertyView
     * <PropertyViewItem title value><Button/></PropertyViewItem>
     *
     * @export
     * @class PropertyViewItem
     * @extends {React.Component<IPropertyViewItemProps, IPropertyViewItemState>}
     */
    export class PropertyViewItem extends React.Component<IPropertyViewItemProps, {}> {
        render(): JSX.Element;
    }
    export interface IPropertyProps {
        title: string;
        description?: string;
        className?: string;
    }
    export class Property extends React.Component<IPropertyProps, {}> {
        render(): JSX.Element;
    }

    // recaptcha
    export interface RecaptchaProps {
        sitekey: string;
        className?: string;
        render?: "explicit" | "onload";
        theme?: string;
        type?: string;
        size?: string;
        id?: string;
        tabindex?: string;
        loadCallbackName?: string;
        verifyCallbackName?: string;
        expiredCallbackName?: string;
        onLoadCallback?: Function;
        onVerifyCallback?: Function;
        onExpiredCallback?: Function;
        setCaptchaId?: Function;
    }
    export class Recaptcha extends React.Component<RecaptchaProps, {}> {
        static defaultProps: RecaptchaProps;
        elementId: string;
        id: string;

        componentDidMount(): void;

        reset(id?: number): void;

        execute(id?: number): void;

        render(): JSX.Element;
    }

    // settings

    export interface ISettingsProps {
    }
    export class Settings extends React.Component<ISettingsProps, {}> {
        render(): JSX.Element;
    }
    export interface ISettingItemProps {
        name: string;
        description?: string;
    }
    export class SettingItem extends React.Component<ISettingItemProps, {}> {
        render(): JSX.Element;
    }

    export interface ISteppersProps {
    }
    export class Stepper extends React.Component<ISteppersProps, {}> {
        render(): JSX.Element;
    }
    export interface ISteppersItemProps {
        number: number;
        title: string;
    }
    export class StepperItem extends React.Component<ISteppersItemProps, {}> {
        render(): JSX.Element;
    }

    // switch

    export interface IToggleProps {
        checked?: boolean;
        defaultChecked?: boolean;
        onChange?: React.FormEventHandler<HTMLInputElement>;
        disabled?: boolean;
    }
    export class Toggle extends React.Component<IToggleProps, {}> {
        componentWillMount(): void;

        render(): JSX.Element;
    }

    // tab-bar
    export interface ITabItem {
        text: string;
        link: string;
    }
    export interface ITabBarProps {
        items: ITabItem[];
    }
    export class TabBar extends React.Component<ITabBarProps, {}> {
        constructor(props: ITabBarProps);

        render(): JSX.Element;
    }

    // text

    export interface ITitleProps extends ComponentFontSize, ComponentBase, ComponentTitle, ComponentColorSheme, ComponentColor {
    }
    export class Title extends React.Component<ITitleProps, {}> {
        static defaultProps: ITitleProps;

        render(): JSX.Element;
    }
    export interface ITextProps extends ComponentFontSize, ComponentBase, ComponentColorSheme, ComponentColor {
    }
    export class Text extends React.Component<ITextProps, {}> {
        static defaultProps: ITextProps;

        render(): JSX.Element;
    }

    // tree-view

    export interface TreeViewContainerProps {
        label: string;
        collapsed?: boolean;
        disabled?: boolean;
        onClick?: Function;
    }
    export interface TreeViewContainerState {
        collapsed?: boolean;
    }
    export class TreeViewContainer extends React.Component<TreeViewContainerProps, TreeViewContainerState> {
        constructor(props: TreeViewContainerProps);

        toggle(): void;

        render(): JSX.Element;
    }

    // date-picker

    export interface DatePickerProps {
        autoSelect?: boolean;
        cancelLabel?: string;
        className?: string;
        defaultDate?: string | Date;
        disableYearSelection?: boolean;
        disabled?: boolean;
        hideCalendarDate?: boolean;
        locale?: string;
        maxDate?: string;
        minDate?: string;
        mode?: "portrait" | "landscape";
        okLabel?: string;
        onChange?: (date: string) => void;
        onDismiss?: () => void;
        onShow?: () => void;
        placeholder?: string;
        showNeighboringMonths?: boolean;
    }
    export interface DatePickerState {
        date: string;
    }

    export class DatePicker extends React.Component<DatePickerProps, DatePickerState> {
        getDate(): string;

        render(): JSX.Element;
    }

    // calendar

    interface CalendarProps {
        autoSelect?: boolean;
        cancelLabel?: string;
        disableYearSelection?: boolean;
        hideCalendarDate?: boolean;
        initialDate?: string;
        locale?: string;
        showNeighboringMonths?: boolean;
        onDayClick?: () => void;
        onAccept?: () => void;
        onDismiss?: () => void;
        maxDate?: string;
        minDate?: string;
        okLabel?: string;
        mode?: "landscape" | "portrait";
    }

    interface CalendarState {
        displayDate: string;
        selectedDate: string;
        displayMonthDay: boolean;
        transitionDirection: "right" | "left" | "up" | "down";
    }

    export class Calendar extends React.Component<CalendarProps, CalendarState> {
        assignedProps: CalendarProps;

        getSelectedDate(): string;

        render(): JSX.Element
    }

    // TimePicker
    export interface TimePickerProps {
        autoSelect?: boolean;
        cancelLabel?: string;
        className?: string;
        defaultTime?: string;
        disabled?: boolean;
        okLabel?: string;
        onChange?: (date: string) => void;
        onDismiss?: () => void;
        onShow?: () => void;
        mode?: "portrait" | "landscape";
    }

    interface TimePickerState {
        time: string;
    }

    export class TimePicker extends React.Component<TimePickerProps, TimePickerState> {
        render(): JSX.Element
    }

    // Clock
    interface ClockProps {
        initialTime: string,
        onChangeHours?: (time: string) => void,
        onChangeMinutes: (time: string) => void,
    }

    interface ClockState {
        mode: "h" | "m";
        selectedTime: string;
    }

    export class Clock extends React.Component<ClockProps, ClockState> {
        setMode(mode: "h" | "m"): void;

        getTime(): string;

        render(): JSX.Element
    }
}
